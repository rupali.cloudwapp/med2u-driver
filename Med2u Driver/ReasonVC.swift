//
//  ReasonVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 04/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ReasonVC: UIViewController {
    
   var ordertypeStr = String()
    var orderideStr = String()

    var customerSignImg = UIImage()
       var driverSignImg = UIImage()
    
    var camefrom = String()
    
    @IBOutlet weak var popup_view: UIView!
    @IBOutlet weak var grad_view: UIView!
    
    
    
    @IBOutlet weak var reason_tv: UITextView!
    
    
    
    @IBOutlet weak var return_btn: UIButton!
    
    @IBAction func ok_act(_ sender: Any) {
        
        
        
        self.popup_view.isHidden = true
        
        self.navigationController?.popToRootViewController(animated: true)

    }
    @IBAction func return_act(_ sender: Any) {
        
        if reason_tv.text == ""
        {
            self.view.showToast(toastMessage: "Please write a reason", duration: 1)
        }
        else
        {
            
            if camefrom == "signature"
            {
                
            }
            else
            {
                ReturnService()

            }
            
        }
        
    }
    
    
    @IBAction func menu_act(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         if #available(iOS 13.0, *) {
                     let app = UIApplication.shared
                     let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                     
                     let statusbarView = UIView()
                     statusbarView.backgroundColor = themeColorFaint
                     view.addSubview(statusbarView)
                   
                     statusbarView.translatesAutoresizingMaskIntoConstraints = false
                     statusbarView.heightAnchor
                         .constraint(equalToConstant: statusBarHeight).isActive = true
                     statusbarView.widthAnchor
                         .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                     statusbarView.topAnchor
                         .constraint(equalTo: view.topAnchor).isActive = true
                     statusbarView.centerXAnchor
                         .constraint(equalTo: view.centerXAnchor).isActive = true
                   
                 } else {
                     let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                     statusBar?.backgroundColor = themeColorFaint
                 }

        let gradientLayer: CAGradientLayer = CAGradientLayer()
                           gradientLayer.frame = view.bounds
                             let topColor: CGColor = themeColorFaint.cgColor
                             let middleColor: CGColor = themeColorDark.cgColor
                            let bottomColor: CGColor = themeColorDark.cgColor
                             gradientLayer.colors = [topColor, middleColor, bottomColor]
                                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                               gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
              
                 self.grad_view.clipsToBounds = true
                            self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
                self.return_btn.layer.insertSublayer(gradientLayer, at: 0)
    }
    func  ReturnService()
          {
               let type = NVActivityIndicatorType.ballClipRotateMultiple
                                      let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                      let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                             self.view.addSubview(activityIndicatorView)
                            self.view.isUserInteractionEnabled = false
               
              
                   activityIndicatorView.startAnimating()
                   var param = [String:Any]()
         

param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
            param["order_id"] = orderideStr
            param["order_type"] = "return"
            param["return_pharmacy_reason"] = self.reason_tv.text as! String

         print("param",param)
                   WebService().postRequest(methodName: delivered_and_return , parameter: param) { (response) in
                       
                       activityIndicatorView.stopAnimating()
                      self.view.isUserInteractionEnabled = true
                       
                       if let newResponse = response as? NSDictionary {
                           if newResponse.value(forKey: "status") as! Bool  == true {
                            
                            
                            self.reason_tv.text = ""
                               
                                                      self.popup_view.isHidden = false

                                   
                               }
                           
                           else {
                               self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                           }
                       }
                       else if let newMsg = response as? String{
                          self.view.showToast(toastMessage: newMsg, duration: 1)
                       }
                       else {
                          self.view.showToast(toastMessage: "No Data Found", duration: 1)

                       }
                   }
               
          }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  EditProfileVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 27/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import MKDropdownMenu
import NVActivityIndicatorView
import AlamofireImage
import GooglePlaces

@available(iOS 13.0, *)
class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource,UIScrollViewDelegate {
    
    var topassLat = String()
       var topassLong = String()
      var sectopassLat = String()
      var sectopassLong = String()
      
      var mediExpirybool = Bool()
        var consExpirybool = Bool()
      var dobbool = Bool()
    let aGMSGeocoder: CLGeocoder = CLGeocoder()

     var tag = 0
    
    @IBOutlet weak var licensee_image: UIImageView!
    var locatioN = CLLocation()

      var pickedImagee = UIImage()
    
    
  //  @IBOutlet weak var license_img: UIImageView!
         
        
      var nameStr = String()

    var vehicletypeArr = ["Car","Motorcycle","Bicycle"]
      
      var thirdpartyArr = ["Yes","No"]
      var insureCTPArr = ["Yes","No"]

      
      var priAddressbool = Bool()
      var secAddressbool = Bool()
    var imgStr = String()

      var placesClient = GMSPlacesClient()
    
    let picker = UIImagePickerController()

    
    

        @IBOutlet weak var noregistration_lbl: UILabel!
        @IBOutlet weak var nopolice_lbl: UILabel!
        @IBOutlet weak var nolicence_lbl: UILabel!
        @IBAction func ok_act(_ sender: Any) {
              self.Ppopup_vc.isHidden = true

           self.navigationController?.popViewController(animated: true)
          }
        @IBOutlet weak var Ppopup_vc: UIView!
        
        
        //MARK : IBOUTLETS:
        
        @IBOutlet weak var name_tf: UITextField!
        
        @IBOutlet weak var email_tf: UITextField!
        
        
        @IBOutlet weak var mobile_tf: UITextField!
        
          
          @IBOutlet weak var addressLine1_tf: UITextField!
          @IBOutlet weak var state_tf: UITextField!
          @IBOutlet weak var postcode_tf: UITextField!
          @IBOutlet weak var suburb_tf: UITextField!
          @IBOutlet weak var vehicletype_tf: UITextField!
          @IBOutlet weak var vehiclemake_tf: UITextField!
          @IBOutlet weak var vehiclemodel_tf: UITextField!
          @IBOutlet weak var bankAcc_tf: UITextField!
          @IBOutlet weak var abn_tf: UITextField!
          @IBOutlet weak var thirdparty_tf: UITextField!
          @IBOutlet weak var ctpEnsured_tf: UITextField!
            
          @IBOutlet weak var vehicleType_dropdown: MKDropdownMenu!
          
          @IBOutlet weak var thirdparty_dropdown: MKDropdownMenu!
          @IBOutlet weak var CTP_dropdown: MKDropdownMenu!
         
          @IBOutlet weak var name_lbl: UILabel!
          @IBOutlet weak var signup_buton: DesignableButton!
          @IBOutlet weak var pro_img: UIImageView!
          @IBOutlet weak var gradient_view: UIView!
          @IBOutlet weak var vehiclemake_lbl: UILabel!
          @IBOutlet weak var vehiclemodel_lbl: UILabel!
        
          @IBOutlet weak var ensure_lbl: UILabel!
          
          
          var appDele = UIApplication.shared.delegate as! AppDelegate

          @IBOutlet weak var scroll_view: UIScrollView!
          
        @IBOutlet weak var police_img: UIImageView!
        
        @IBOutlet weak var registartion_img: UIImageView!
        
        var licenseBool = Bool()
           var policeBool = Bool()
           var regisBool = Bool()
        
    @IBOutlet weak var vw2: UIView!
    @IBOutlet weak var vw3: UIView!
    @IBOutlet weak var vw1: UIView!
    //MARK : IBACTIONS:
    
      @IBAction func license_act(_ sender: Any) {
            
            licenseBool = true
            policeBool = false
            regisBool = false

          openFileAttachment ()
               
           }
        
        @IBAction func policeChk_act(_ sender: Any) {
            
            licenseBool = false
                 policeBool = true
                 regisBool = false
    openFileAttachment ()
                  
              }
        @IBAction func Registration_act(_ sender: Any) {
            
            licenseBool = false
                        policeBool = false
                        regisBool = true

           openFileAttachment ()
              }
    
    
    
    
    
    
    
    
         
          @IBAction func address1_act(_ sender: Any) {
              priAddressbool = true
                            secAddressbool = false
                      
                      
                      let autocompleteController = GMSAutocompleteViewController()
                              autocompleteController.delegate = self
                              present(autocompleteController, animated: true, completion: nil)
           
          }
          
        @IBAction func pro_act(_ sender: Any) {
           
            openFileAttachment ()

              
           }
           
           @IBAction func BACK(_ sender: Any) {
            
            self.vehicleType_dropdown.closeAllComponents(animated: true)
                        self.thirdparty_dropdown.closeAllComponents(animated: true)
                        self.CTP_dropdown.closeAllComponents(animated: true)
              
      self.navigationController?.popViewController(animated: true)
            
                }
    @IBAction func signup_act(_ sender: Any) {
           
          
           self.vehicleType_dropdown.closeAllComponents(animated: true)
           self.thirdparty_dropdown.closeAllComponents(animated: true)
           self.CTP_dropdown.closeAllComponents(animated: true)

            if addressLine1_tf.text!.isEmpty {
                  
                  self.view.showToast(toastMessage:  "Address Line1 Required!!", duration: 1)
                  
                  }
          
               else if suburb_tf.text!.isEmpty {
                          
                          self.view.showToast(toastMessage:  "Suburb/city Required!!", duration: 1)
                          
                          }
                   else if state_tf.text!.isEmpty {
                                 
                                 self.view.showToast(toastMessage:  "State Required!!", duration: 1)
                                 
                                 }
           else if postcode_tf.text!.isEmpty {
                                
                self.view.showToast(toastMessage:  "Postcode Required!!", duration: 1)
                                
                                }
             
           
          
     else
           {
               
              
  UpdateDriverProfile()
           //signupDriver()
           
           }
       }
        
        

        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            self.Ppopup_vc.isHidden = true

            
            name_lbl.isHidden = true

            if #available(iOS 13.0, *) {
                           let app = UIApplication.shared
                           let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                           
                           let statusbarView = UIView()
                           statusbarView.backgroundColor = themeColorFaint
                           view.addSubview(statusbarView)
                         
                           statusbarView.translatesAutoresizingMaskIntoConstraints = false
                           statusbarView.heightAnchor
                               .constraint(equalToConstant: statusBarHeight).isActive = true
                           statusbarView.widthAnchor
                               .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                           statusbarView.topAnchor
                               .constraint(equalTo: view.topAnchor).isActive = true
                           statusbarView.centerXAnchor
                               .constraint(equalTo: view.centerXAnchor).isActive = true
                         
                       } else {
                           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                           statusBar?.backgroundColor = themeColorFaint
                       }
            
         //   self.Ppopup_vc.isHidden = true
            
            self.picker.delegate = self
            
            self.scroll_view.delegate = self

            
            self.vehicleType_dropdown.delegate = self
            self.vehicleType_dropdown.dataSource = self
            
            self.thirdparty_dropdown.delegate = self
                   self.thirdparty_dropdown.dataSource = self
            
            self.CTP_dropdown.delegate = self
                   self.CTP_dropdown.dataSource = self
            
             
            self.suburb_tf.isUserInteractionEnabled = false
            self.state_tf.isUserInteractionEnabled = false
            self.postcode_tf.isUserInteractionEnabled = false
            self.email_tf.isUserInteractionEnabled = false

           
            
            self.vw1.clipsToBounds = true
            self.vw2.clipsToBounds = true
            self.vw3.clipsToBounds = true

            
       self.vw1.applyGradient(colours: [themeColorFaint, themeColorDark])
       self.vw2.applyGradient(colours: [themeColorFaint, themeColorDark])
       self.vw3.applyGradient(colours: [themeColorFaint, themeColorDark])

             DriverDetails()

       
        }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.vehicleType_dropdown.closeAllComponents(animated: true)
        self.thirdparty_dropdown.closeAllComponents(animated: true)
        self.CTP_dropdown.closeAllComponents(animated: true)
    }
        override func viewWillAppear(_ animated: Bool) {
            
//            addressLine1_tf.isUserInteractionEnabled = false
//            state_tf.isUserInteractionEnabled = false
//            postcode_tf.isUserInteractionEnabled = false
//            vehicletype_tf.isUserInteractionEnabled = false
//            vehiclemake_tf.isUserInteractionEnabled = false
//            vehiclemodel_tf.isUserInteractionEnabled = false
//            bankAcc_tf.isUserInteractionEnabled = false
//            abn_tf.isUserInteractionEnabled = false
//            thirdparty_tf.isUserInteractionEnabled = false
//            ctpEnsured_tf.isUserInteractionEnabled = false
//            vehicleType_dropdown.isUserInteractionEnabled = false
//            thirdparty_dropdown.isUserInteractionEnabled = false
//            CTP_dropdown.isUserInteractionEnabled = false
//            name_tf.isUserInteractionEnabled = false
//            email_tf.isUserInteractionEnabled = false
//            mobile_tf.isUserInteractionEnabled = false

        //    self.signup_buton.isHidden = false
            
            
                
             //For setting grdaient :-
                                              
                     self.signup_buton.clipsToBounds = true
                   let gradientLayer: CAGradientLayer = CAGradientLayer()
                       gradientLayer.frame = view.bounds
                            let topColor: CGColor = themeColorFaint.cgColor
                         let middleColor: CGColor = themeColorDark.cgColor
                                 let bottomColor: CGColor = themeColorDark.cgColor
                             gradientLayer.colors = [topColor, middleColor, bottomColor]
                            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                     gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                   
                       
                       self.gradient_view.clipsToBounds = true
                                                                
                  self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
            self.signup_buton.layer.insertSublayer(gradientLayer, at: 0)


            
        }
        
        func  DriverDetails()
                        {
                             let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                    let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                    let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                           self.view.addSubview(activityIndicatorView)
                                          self.view.isUserInteractionEnabled = false
                             
                            
                                 activityIndicatorView.startAnimating()
                                 var param = [String:Any]()
                     
                          param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                           
                           
                                 WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                                     
                                     activityIndicatorView.stopAnimating()
                                    self.view.isUserInteractionEnabled = true
                                     
                                     if let newResponse = response as? NSDictionary {
                                         if newResponse.value(forKey: "status") as! Bool  == true {
                                          
                                          let userData = newResponse.value(forKey: "data") as! NSDictionary
                                            
                                          print("userData",userData)
                                            
                                            self.addressLine1_tf.text = userData.value(forKey: "address1") as! String
                                            
                                          self.state_tf.text = userData.value(forKey: "state") as! String
                                            
                                            self.suburb_tf.text = userData.value(forKey: "suburb") as! String
                                            
                                            self.postcode_tf.text = userData.value(forKey: "postcode") as! String
                                            
                                            
                                            
                                            
                                            self.topassLat = userData.value(forKey: "latitude") as! String
                                              self.topassLong = userData.value(forKey: "longitude") as! String
                                              
                               let vehicle_model = userData.value(forKey: "vehicle_model") as! String
                                   if vehicle_model == "Car"
                                              {
                                                  
                                                self.vehiclemake_lbl.text = "Vehicle Make/Year"
                                                self.vehiclemodel_lbl.text = "Vehicle Model"
                                                  
                                                  
                                                self.vehiclemake_tf.placeholder = "Vehicle Make/Year"
                                                self.vehiclemodel_tf.placeholder = "Vehicle Model"
                                                  
                                              }
                                            
                                            else  if vehicle_model == "Motorcycle"
                                              {
                                                  
                                                self.vehiclemake_lbl.text = "Motorcycle Make/Year"
                                                self.vehiclemodel_lbl.text = "Motorcycle Model"
                                                  
                                                  
                                                self.vehiclemake_tf.placeholder = "Motorcycle Make/Year"
                                                self.vehiclemodel_tf.placeholder = "Motorcycle Model"
                                                  
                                              }
                                              else
                                              {
                                                self.vehiclemake_lbl.text = "License Number"
                                                self.vehiclemodel_lbl.text = "BSB"
                                                                 
                                                self.vehiclemake_tf.placeholder = "License Number"
                                                self.vehiclemodel_tf.placeholder = "BSB"
                                                  
                                              }
                                            
                                            
                                            
                                              self.vehicletype_tf.text = userData.value(forKey: "vehicle_model") as! String
                                            self.vehiclemake_tf.text = userData.value(forKey: "vehicle_make") as! String
                                                                                    self.vehiclemodel_tf.text = userData.value(forKey: "license_no") as! String
                                            
                                            self.bankAcc_tf.text = userData.value(forKey: "bank_account_no") as! String
                                            self.abn_tf.text = userData.value(forKey: "abn") as! String
                                            self.thirdparty_tf.text = userData.value(forKey: "hold_third_party") as! String

                                         self.ctpEnsured_tf.text = userData.value(forKey: "insure_your_car") as! String
                                            self.name_tf.text = userData.value(forKey: "name") as! String
                                            self.email_tf.text = userData.value(forKey: "email") as! String
                                            self.mobile_tf.text = userData.value(forKey: "mobile") as! String
                                            
                                            
                                            
                                            
                                            
                                            let certificate_of_registration = userData.value(forKey: "certificate_of_registration") as! String
    let police_check_certificate = userData.value(forKey: "police_check_certificate") as! String
                                          
                                            let copy_driving_record = userData.value(forKey: "copy_driving_record") as! String
                                            
             if certificate_of_registration == ""
             {
                self.noregistration_lbl.text = "No copy of Registration uploaded"
                
                                            }
            else
             {
                self.noregistration_lbl.text = ""

                                            }
        
                                            if copy_driving_record == ""
                                                    {
                                                        self.nolicence_lbl.text = "No copy of Driving Licence uploaded"
                                                       
                                                                                   }
                                                   else
                                                    {
                                                        self.nolicence_lbl.text = ""
                                                                                   }
                                            if copy_driving_record == ""
                                                                                           {
                                                                                               self.nolicence_lbl.text = "No copy of Licence uploaded"
                                                                                              
                                                                                                                          }
                                                                                          else
                                                                                           {
                                                                                               self.nolicence_lbl.text = ""
                                                                                                                          }
                                            if police_check_certificate == ""
                                             {
                                                 self.nopolice_lbl.text = "No copy of Police Check Certificate uploaded"
                                                
                                                                            }
                                            else
                                             {
                                                 self.nopolice_lbl.text = ""
                                                                            }
                                            
                                            
                                            
                                            let image = userData.value(forKey: "image") as! String
                                                                                  

                                         
                                            self.pro_img.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
                                            
                                         self.licensee_image.af_setImage(withURL: URL(string: "\(img_Url)\(copy_driving_record)")!)
                                            
     self.registartion_img.af_setImage(withURL: URL(string: "\(img_Url)\(certificate_of_registration)")!)
                                            
      self.police_img.af_setImage(withURL: URL(string: "\(img_Url)\(police_check_certificate)")!)
                                            
                                            
                                             }
                                      else {
                                          
                                  self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                    }
                                         }
                                      
                                     else {
                                        self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                     }
                                 }
                             
                        }
                  //METHOD FOR IMAGE PICKING:
                     
                   
                     func openFileAttachment () {
                         self.view.endEditing(true)
                         let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                         actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                             
                             self.openCamera()
                         }))
                         actionSheet.addAction(UIAlertAction(title:"Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
                             
                             self.openGallery()
                         }))
                         
                         actionSheet.addAction(UIAlertAction(title:"Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                         self.present(actionSheet, animated: true, completion: nil)
                     }
                     
                     
                     func openCamera() {
                         if UIImagePickerController.isSourceTypeAvailable(.camera){
                             picker.allowsEditing = false
                             picker.sourceType = .camera
                             picker.cameraCaptureMode = .photo
                             present(picker, animated: true, completion: nil)
                         }
                         else{
                             let alert = UIAlertController(title: "Alert", message: "No Camera found in your Device", preferredStyle: UIAlertController.Style.alert)
                             alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                             self.present(alert, animated: true, completion: nil)
                         }
                     }
                     
                     func openGallery() {
                         picker.allowsEditing = true
                         picker.sourceType = .photoLibrary
                         // present(picker, animated: true, completion: nil)
                         
                         self.present(picker, animated: true, completion: nil)
                     }
                     // MARK: - ImagePicker Method
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
                         if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                             
                             if   licenseBool == true
                             {
                               licensee_image.image = pickedImage
                               
                               }
                              else if   policeBool == true
                               {
                                 police_img.image = pickedImage
                                 
                                 }
                               else if   regisBool == true
                             {
                               registartion_img.image = pickedImage

                               }
                            else
                            {
                               // self.Ppopup_vc.isHidden = false

                                self.pickedImagee = pickedImage
                                
                                self.pro_img.image = pickedImage
                              
                                self.imgStr = "\(pickedImage)"
                            }
                             
                            
                         }
                         
                        picker.dismiss(animated: true, completion: nil)
                     }
                     
                     
                     func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
                         dismiss(animated: true, completion: nil)
                     }
                     
    //MARK: GMSAUTOPICKER DELEGATE METHODS
                     
                     
                     func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
                         viewController.dismiss(animated: true, completion: nil)
                        
    //
    //                    DataService.instance.place = place
    //                    FillAddress(place: place)
    //                    fillAddressForm()
    //                    print(DataService.instance._address_line1)
    //                    print(DataService.instance._city)
    //                    print(DataService.instance._postalCode)
    //                    print(DataService.instance._state)
    //                    print(DataService.instance._country)
    //                    DataService.instance.addressLabel = place.formattedAddress
                          
                         
                      locatioN = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                        
                         print("Place name \(place.name)")
                         print("Place address \(place.formattedAddress)")
                         print("Place attributions \(place.placeID)")
                         print("Place attributions \(place.coordinate.latitude))")
                        
                
                    self.addressLine1_tf.text = "\(place.formattedAddress ?? "indore")"
                    
                 
                    self.topassLat = "\(place.coordinate.latitude)"
                    self.topassLong = "\(place.coordinate.longitude)"
                    
                    getAddressFromLocation(location: locatioN)
                      
                         
                         print("Place attributions \(place.coordinate.longitude))")
                      //   getCategory()

                         
                         
                     }
        
        
     
                     
                     func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
                         print("error")
                     }
                     
                     func wasCancelled(_ viewController: GMSAutocompleteViewController) {
                         
                         viewController.dismiss(animated: true, completion: nil)
                         
                         print("No place selected")
                     }
           
      func getAddressFromLocation(location: CLLocation) {
        
        

       
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            print(location)
            
           
            if error != nil {
              return
            }

            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]

            if placeMark == nil {
                return
            }

            
            if let city = placeMark.addressDictionary!["City"] as? String {
                print(city, terminator: "")
                self.suburb_tf.text = city
            }

           
            
            // state
            if let state = placeMark.addressDictionary!["State"] as? String {
                self.state_tf.text = state
            }
            // Zip
            if let zip = placeMark.addressDictionary!["Zip"] as? String {
                self.postcode_tf.text = zip
            }
                            
                                                let zipCode = placeMark.postalCode ?? "unknown"
                                                self.postcode_tf.text = zipCode
            
            


        })

        }
    
    
    
    //METHOD FOR MKDROPDOWN :

       
       func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
              return 1
          }
          
          func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
           
           if dropdownMenu == vehicleType_dropdown
           {

               return vehicletypeArr.count
               
           }
           else if dropdownMenu == CTP_dropdown
           {
               return insureCTPArr.count

           }
           
           return thirdpartyArr.count
           
           
          }
         func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
           
           if dropdownMenu == vehicleType_dropdown
           {

               return vehicletypeArr[row]
               
           }
           else if dropdownMenu == CTP_dropdown
           {
               return insureCTPArr[row]

           }
           else
           {
           
           return thirdpartyArr[row]
           
           }
           
           
        }
        
        func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
           
           
           if dropdownMenu == vehicleType_dropdown
           {
    
               vehicletype_tf.text = vehicletypeArr[row]
               
                 var vehicletypeArr = ["Car","Motorcycle","Bicycle"]
               
               if vehicletype_tf.text == "Car"
               {
                   
                   vehiclemake_lbl.text = "Vehicle Make/Year"
                   vehiclemodel_lbl.text = "Vehicle Model"
                   
                   
                   vehiclemake_tf.placeholder = "Vehicle Make/Year"
                   vehiclemodel_tf.placeholder = "Vehicle Model"
                   
               }
             
             else  if vehicletype_tf.text == "Motorcycle"
               {
                   
                   vehiclemake_lbl.text = "Motorcycle Make/Year"
                   vehiclemodel_lbl.text = "Motorcycle Model"
                   
                   
                   vehiclemake_tf.placeholder = "Motorcycle Make/Year"
                   vehiclemodel_tf.placeholder = "Motorcycle Model"
                   
               }
               else
               {
                   vehiclemake_lbl.text = "License Number"
                                  vehiclemodel_lbl.text = "BSB"
                                  
               vehiclemake_tf.placeholder = "License Number"
              vehiclemodel_tf.placeholder = "BSB"
                   
               }
           
               
               vehiclemodel_tf.text = ""
            vehiclemake_tf.text = ""

               
               vehicleType_dropdown.closeAllComponents(animated: true)
               
           }
           else if dropdownMenu == CTP_dropdown
           {
                ctpEnsured_tf.text = insureCTPArr[row]
                     
               CTP_dropdown.closeAllComponents(animated: true)

           }
           else
           {
           
           thirdparty_tf.text = thirdpartyArr[row]
                 
                   thirdparty_dropdown.closeAllComponents(animated: true)
           
           }
           
           
               
           
           
           
        }
    
    
    func  UpdateDriverProfile()
         {
             let type = NVActivityIndicatorType.ballClipRotateMultiple
                       let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                       let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
              self.view.addSubview(activityIndicatorView)
             self.view.isUserInteractionEnabled = false
                 activityIndicatorView.startAnimating()
          
          
                  var param = [String:Any]()
            
       param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")

            param["name"] = self.name_tf.text as! String

         //   param["email"] = self.email_tf.text as! String
             param["mobile"] = self.mobile_tf.text as! String
           param["suburb"] = self.suburb_tf.text as! String
                    param["city"] = self.suburb_tf.text as! String
                    param["state"] = self.state_tf.text as! String
                    param["postcode"] = self.postcode_tf.text as! String
         
             param["address"] = self.addressLine1_tf.text as! String
            
             param["latitude"] = topassLat
             param["longitude"] = topassLong
      
           param["vehicle_make"] = self.vehiclemake_tf.text as! String
           param["vehicle_model"] = self.vehicletype_tf.text as! String
           param["plate_no"] = self.vehiclemake_tf.text as! String
           param["license_no"] = self.vehiclemodel_tf.text as! String
           param["bank_account_no"] = self.bankAcc_tf.text as! String
           
            param["abn"] = abn_tf.text as! String
           param["bsb"] = self.vehiclemodel_tf.text as! String

           param["hold_third_party"] = self.thirdparty_tf.text as! String
           param["insure_your_car"] = self.ctpEnsured_tf.text as! String

        
//             param["fcm_token"] = "abcd"
//             param["device_type"] = "IOS"
//          param["device_id"] =  UserDefaults.standard.value(forKey: "decviceId") as! String
          
           param["image"] = UIImageJPEGRepresentation(pro_img.image!, 1)!
            
//            copy_driving_record:xxxxx(image)
//            police_check_certificate:xxxxx(image)
//            pink_green_slip:xxxxx (image)
//            certificate_of_registration:xxxx(image)
            
            
            
            param["police_check_certificate"] = police_img.image!.compressTo(1)
           param["certificate_of_registration"] = registartion_img.image!.compressTo(1)
            param["copy_driving_record"] = licensee_image.image!.compressTo(1)
        //param["pink_green_slip"] = UIImageJPEGRepresentation(police_img.image!, 1)!


        WebService().uploadImagesHandler(methodName: update_profile , parameter: param) { (response) in
                                   
                          activityIndicatorView.stopAnimating()
                      self.view.isUserInteractionEnabled = true

            if let newResponse = response as? NSDictionary {
                              if newResponse.value(forKey: "status") as! Bool  == true {
                                
                             
                                self.Ppopup_vc.isHidden = false
                                
                

                                
                  if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                                              
                           print("userData",userData)
                                                                        
                          let userid = userData.value(forKey: "id") as! String
                                                                 
                                                                        
                        UserDefaults.standard.set(userid, forKey: "userIdLogin")
                                                                    UserDefaults.standard.set(userData, forKey: "userData")
                   
                   
                   
               //    self.Ppopup_vc.isHidden = false
                   
                  self.view.showToast(toastMessage: "Profile Updated Successfully", duration: 1)
                      /* if #available(iOS 13.0, *) {
                                  let scene = UIApplication.shared.connectedScenes.first
                      if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                          
                          sd.setHomeRootController()
                          
                                  }
                                   
                       }
                                   
                       else
                       {

                          self.appDele.setHomeRootController()

                      }*/

                      
                                  }
                                       }
                                          else
                              {

                                  self.view.showToast(toastMessage: "Failed to update", duration: 1)

                                          }
                                       
                                  }
                                   
                                   else {
                                         self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                         
                                   }
                               }
                               }
       
       
       
       
    
    
    
    
    
    
    
    
    
    
    
    
    }

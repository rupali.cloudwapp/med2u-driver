//
//  PersonalInfoVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import GooglePlaces
import NVActivityIndicatorView
import MKDropdownMenu


@available(iOS 13.0, *)
class PersonalInfoVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource,UIScrollViewDelegate {
   
    let aGMSGeocoder: CLGeocoder = CLGeocoder()

  var tag = 0
    
    
    @IBAction func ok_act(_ sender: Any) {
        self.Ppopup_vc.isHidden = true
        pro_img.image = pickedImagee

        
    }
    @IBOutlet weak var Ppopup_vc: UIView!
    
   var pickedImagee = UIImage()
    
    var nameStr = String()

  var vehicletypeArr = ["Car","Motorcycle","Bicycle"]
    
    var thirdpartyArr = ["Yes","No"]
    var insureCTPArr = ["Yes","No"]

    
    var priAddressbool = Bool()
    var secAddressbool = Bool()

    var placesClient = GMSPlacesClient()
    
     //MARK : IBOUTLETS:
    
    @IBOutlet weak var addressLine1_tf: UITextField!
    @IBOutlet weak var state_tf: UITextField!
    @IBOutlet weak var postcode_tf: UITextField!
    @IBOutlet weak var suburb_tf: UITextField!
    @IBOutlet weak var vehicletype_tf: UITextField!
    @IBOutlet weak var vehiclemake_tf: UITextField!
    @IBOutlet weak var vehiclemodel_tf: UITextField!
    @IBOutlet weak var bankAcc_tf: UITextField!
    @IBOutlet weak var abn_tf: UITextField!
    @IBOutlet weak var thirdparty_tf: UITextField!
    @IBOutlet weak var ctpEnsured_tf: UITextField!
      
    @IBOutlet weak var vehicleType_dropdown: MKDropdownMenu!
    
    @IBOutlet weak var thirdparty_dropdown: MKDropdownMenu!
    @IBOutlet weak var CTP_dropdown: MKDropdownMenu!
   
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var signup_buton: DesignableButton!
    @IBOutlet weak var pro_img: UIImageView!
    @IBOutlet weak var gradient_view: UIView!
    @IBOutlet weak var vehiclemake_lbl: UILabel!
    @IBOutlet weak var vehiclemodel_lbl: UILabel!
  
    @IBOutlet weak var ensure_lbl: UILabel!
    
    
    var locatioN = CLLocation()
    
    
    @IBOutlet weak var scroll_view: UIScrollView!
    
    //MARK : IBACTIONS:
    
  
    
   
    @IBAction func address1_act(_ sender: Any) {
        
        priAddressbool = true
              secAddressbool = false
        
        
        let autocompleteController = GMSAutocompleteViewController()
                autocompleteController.delegate = self
                present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func signup_act(_ sender: Any) {
        
       
        self.vehicleType_dropdown.closeAllComponents(animated: true)
        self.thirdparty_dropdown.closeAllComponents(animated: true)
        self.CTP_dropdown.closeAllComponents(animated: true)

         if addressLine1_tf.text!.isEmpty {
               
               self.view.showToast(toastMessage:  "Address Line1 Required!!", duration: 1)
               
               }
       
            else if suburb_tf.text!.isEmpty {
                       
                       self.view.showToast(toastMessage:  "Suburb/city Required!!", duration: 1)
                       
                       }
                else if state_tf.text!.isEmpty {
                              
                              self.view.showToast(toastMessage:  "State Required!!", duration: 1)
                              
                              }
        else if postcode_tf.text!.isEmpty {
                             
             self.view.showToast(toastMessage:  "Postcode Required!!", duration: 1)
                             
                             }
            else if vehicletype_tf.text!.isEmpty {
                               
               self.view.showToast(toastMessage:  "Vehicle Type Required!!", duration: 1)
                               
                               }
            else if vehiclemake_tf.text!.isEmpty {
                                          
                          self.view.showToast(toastMessage:  "Vehicle Make Required!!", duration: 1)
                                          
                                          }
             else if vehiclemodel_tf.text!.isEmpty {
                                          
                          self.view.showToast(toastMessage:  "Vehicle Model Required!!", duration: 1)
                                          
                                          }
            else if ctpEnsured_tf.text!.isEmpty {
                                                     
                                     self.view.showToast(toastMessage:  "Ensure your vehicle filed Required!!", duration: 1)
                                                     
                                                     }
            else if thirdparty_tf.text!.isEmpty {
                                                     
                                     self.view.showToast(toastMessage:  "hold third party field Required!!", duration: 1)
                                                     
                                                     }
  else
        {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UploadDocsVC") as! UploadDocsVC
                
                vc.emailStr = emailStr
                vc.mobileStr = mobileStr
                vc.PwStr = PwStr
                vc.nameStr = nameStr
            
            
            vc.pickedImagee = pro_img.image!
            
            vc.addressLine1 = self.addressLine1_tf.text as! String
            vc.state = self.state_tf.text as! String
            vc.postcode = self.postcode_tf.text as! String
            vc.suburb = self.suburb_tf.text as! String
            vc.vehicletyp = self.vehicletype_tf.text as! String
            vc.vehiclemake = self.vehiclemake_tf.text as! String
            vc.vehiclemodel = self.vehiclemodel_tf.text as! String
            vc.bankAcc = self.bankAcc_tf.text as! String
            vc.thirdparty = self.thirdparty_tf.text as! String
            vc.ctpEnsured = self.ctpEnsured_tf.text as! String
            vc.topassLat = topassLat
            vc.topassLong = topassLong

                self.navigationController?.pushViewController(vc, animated: true)
            
             
            
 
        //signupDriver()
        
        }
    }
    
    @IBAction func pro_act(_ sender: Any) {
        
        openFileAttachment ()
    }
    
    @IBAction func BACK(_ sender: Any) {
        self.vehicleType_dropdown.closeAllComponents(animated: true)
               self.thirdparty_dropdown.closeAllComponents(animated: true)
               self.CTP_dropdown.closeAllComponents(animated: true)

             self.navigationController?.popViewController(animated: true)
         }
    
    
   //MARK : IBDECLARATIONS:
    
    var appDele = UIApplication.shared.delegate as! AppDelegate

     var topassLat = String()
     var topassLong = String()
    var sectopassLat = String()
    var sectopassLong = String()
    
    var mediExpirybool = Bool()
      var consExpirybool = Bool()
    var dobbool = Bool()
      
    var imgStr = String()
          var emailStr = String()
                var mobileStr = String()
                var PwStr = String()
             var stateStr = String()
             var pstcodeStr = String()
             var carenameStr = String()
             var carephoneStr = String()
             var secondAddressStr = String()
             var doctorDetailsStr = String()
    
  let picker = UIImagePickerController()
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       if #available(iOS 13.0, *) {
                      let app = UIApplication.shared
                      let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                      
                      let statusbarView = UIView()
                      statusbarView.backgroundColor = themeColorFaint
                      view.addSubview(statusbarView)
                    
                      statusbarView.translatesAutoresizingMaskIntoConstraints = false
                      statusbarView.heightAnchor
                          .constraint(equalToConstant: statusBarHeight).isActive = true
                      statusbarView.widthAnchor
                          .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                      statusbarView.topAnchor
                          .constraint(equalTo: view.topAnchor).isActive = true
                      statusbarView.centerXAnchor
                          .constraint(equalTo: view.centerXAnchor).isActive = true
                    
                  } else {
                      let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                      statusBar?.backgroundColor = themeColorFaint
                  }
        
        self.scroll_view.delegate = self

        
        self.vehicleType_dropdown.delegate = self
        self.vehicleType_dropdown.dataSource = self
        
        self.thirdparty_dropdown.delegate = self
               self.thirdparty_dropdown.dataSource = self
        
        self.CTP_dropdown.delegate = self
               self.CTP_dropdown.dataSource = self
        
         
        self.suburb_tf.isUserInteractionEnabled = false
        self.state_tf.isUserInteractionEnabled = false
        self.postcode_tf.isUserInteractionEnabled = false

      
        
        //For setting grdaient :-
                               
                                      self.signup_buton.clipsToBounds = true
                                      let gradientLayer: CAGradientLayer = CAGradientLayer()
                                      gradientLayer.frame = view.bounds
                                      let topColor: CGColor = themeColorFaint.cgColor
                                      let middleColor: CGColor = themeColorDark.cgColor
                                    let bottomColor: CGColor = themeColorDark.cgColor
                                      gradientLayer.colors = [topColor, middleColor, bottomColor]
                                      gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                      gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
     //   self.signup_buton.layer.insertSublayer(gradientLayer, at: 0))
        
        self.signup_buton.applyGradient(colours: [themeColorFaint, themeColorDark])
        
        self.signup_buton.setTitle("Next", for: .normal)
        
        self.gradient_view.clipsToBounds = true
                                                 
                                                  self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        self.name_lbl.isHidden = true
        
        picker.delegate = self
       
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.vehicleType_dropdown.closeAllComponents(animated: true)
        self.thirdparty_dropdown.closeAllComponents(animated: true)
        self.CTP_dropdown.closeAllComponents(animated: true)
    }
    //METHOD FOR MKDROPDOWN :

    
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
           return 1
       }
       
       func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        
        if dropdownMenu == vehicleType_dropdown
        {

            return vehicletypeArr.count
            
        }
        else if dropdownMenu == CTP_dropdown
        {
            return insureCTPArr.count

        }
        
        return thirdpartyArr.count
        
        
       }
      func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if dropdownMenu == vehicleType_dropdown
        {

            return vehicletypeArr[row]
            
        }
        else if dropdownMenu == CTP_dropdown
        {
            return insureCTPArr[row]

        }
        else
        {
        
        return thirdpartyArr[row]
        
        }
        
        
     }
     
     func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        
        
        if dropdownMenu == vehicleType_dropdown
        {
 
            vehicletype_tf.text = vehicletypeArr[row]
            
              var vehicletypeArr = ["Car","Motorcycle","Bicycle"]
            
            if vehicletype_tf.text == "Car"
            {
                
                vehiclemake_lbl.text = "Vehicle Make/Year"
                vehiclemodel_lbl.text = "Vehicle Model"
                
                
                vehiclemake_tf.placeholder = "Vehicle Make/Year"
                vehiclemodel_tf.placeholder = "Vehicle Model"
                
            }
          
          else  if vehicletype_tf.text == "Motorcycle"
            {
                
                vehiclemake_lbl.text = "Motorcycle Make/Year"
                vehiclemodel_lbl.text = "Motorcycle Model"
                
                
                vehiclemake_tf.placeholder = "Motorcycle Make/Year"
                vehiclemodel_tf.placeholder = "Motorcycle Model"
                
            }
            else
            {
                vehiclemake_lbl.text = "License Number"
                               vehiclemodel_lbl.text = "BSB"
                               
            vehiclemake_tf.placeholder = "License Number"
           vehiclemodel_tf.placeholder = "BSB"
                
            }
        
            
            vehiclemodel_tf.text = ""
         vehiclemake_tf.text = ""

            
            vehicleType_dropdown.closeAllComponents(animated: true)
            
        }
        else if dropdownMenu == CTP_dropdown
        {
             ctpEnsured_tf.text = insureCTPArr[row]
                  
            CTP_dropdown.closeAllComponents(animated: true)

        }
        else
        {
        
        thirdparty_tf.text = thirdpartyArr[row]
              
                thirdparty_dropdown.closeAllComponents(animated: true)
        
        }
        
        
            
        
        
        
     }
    
    
    //METHOD FOR IMAGE PICKING:
    
  
    func openFileAttachment () {
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title:"Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.openGallery()
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            picker.allowsEditing = false
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "No Camera found in your Device", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        // present(picker, animated: true, completion: nil)
        
        self.present(picker, animated: true, completion: nil)
    }
    // MARK: - ImagePicker Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            Ppopup_vc.isHidden = false
            
            
            pickedImagee = pickedImage
             
            imgStr = "\(pickedImage)"
            
           
        }
        
       picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Webservice Calling

   
                                 
            
    
    //MARK: GMSAUTOPICKER DELEGATE METHODS
                 
                 
                 func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
                     viewController.dismiss(animated: true, completion: nil)
                    
//
//                    DataService.instance.place = place
//                    FillAddress(place: place)
//                    fillAddressForm()
//                    print(DataService.instance._address_line1)
//                    print(DataService.instance._city)
//                    print(DataService.instance._postalCode)
//                    print(DataService.instance._state)
//                    print(DataService.instance._country)
//                    DataService.instance.addressLabel = place.formattedAddress
                      
                     
                  locatioN = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                    
                     print("Place name \(place.name)")
                     print("Place address \(place.formattedAddress)")
                     print("Place attributions \(place.placeID)")
                     print("Place attributions \(place.coordinate.latitude))")
                    
            
                self.addressLine1_tf.text = "\(place.formattedAddress ?? "indore")"
                
             
                self.topassLat = "\(place.coordinate.latitude)"
                self.topassLong = "\(place.coordinate.longitude)"
                
                getAddressFromLocation(location: locatioN)
                  
                     
                     print("Place attributions \(place.coordinate.longitude))")
                  //   getCategory()

                     
                     
                 }
    
    
 
                 
                 func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
                     print("error")
                 }
                 
                 func wasCancelled(_ viewController: GMSAutocompleteViewController) {
                     
                     viewController.dismiss(animated: true, completion: nil)
                     
                     print("No place selected")
                 }
       
  func getAddressFromLocation(location: CLLocation) {
    
    

   
    CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
        print(location)
        
       
        if error != nil {
          return
        }

        var placeMark: CLPlacemark!
        placeMark = placemarks?[0]

        if placeMark == nil {
            return
        }

        
        if let city = placeMark.addressDictionary!["City"] as? String {
            print(city, terminator: "")
            self.suburb_tf.text = city
        }

       
        
        // state
        if let state = placeMark.addressDictionary!["State"] as? String {
            self.state_tf.text = state
        }
        // Zip
        if let zip = placeMark.addressDictionary!["Zip"] as? String {
            self.postcode_tf.text = zip
        }
                        
                                            let zipCode = placeMark.postalCode ?? "unknown"
                                            self.postcode_tf.text = zipCode
        
        


    })

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

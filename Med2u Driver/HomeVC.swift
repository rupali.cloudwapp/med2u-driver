//
//  HomeVC.swift
//  MED 2U
//
//  Created by CW-21 on 06/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation

@available(iOS 13.0, *)
class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate {
    
    var timer = Timer()

    
    //MARK : IBOUTLETS:
    
    
    @IBOutlet weak var lower_view: UIView!
    @IBOutlet weak var Accept_btn: UIButton!
    @IBOutlet weak var currTime_lbl: UILabel!
    @IBOutlet weak var currDate_lbl: UILabel!
    @IBOutlet weak var grad_view: UIView!
    @IBOutlet weak var Table_view: UITableView!
    
    @IBOutlet weak var notApprovedMsg_lbl: UILabel!
    @IBOutlet weak var welcome_lbl: UILabel!
    
    
    var orderidArr = [String]()
    var selectindArr = [Int]()
    var mutArr = NSMutableArray()

    var loadBool = false

      //MARK : IBACTIONS:
    
    @IBAction func accept_act(_ sender: Any) {
        
        if orderidArr.count < 5
        {
            
       self.view.showToast(toastMessage: "Please select Equal or More than 5 Orders", duration: 1)
        }
        else
        {
             AcceptOrder()
        }
        
    }
    @IBAction func menu_act(_ sender: Any) {
        sideMenuController?.toggle()
    }
    
    @IBAction func loc_act(_ sender: Any) {
    }
    //MARK : IBDECLARATIONS:
    
    var HomeData = NSArray()
    var latStr = String()
    var longStr = String()
    var locationManager = CLLocationManager()
    var currentLoc: CLLocation!
    
    var selectedindex = -1
    var deselectedindex = -1


    override func viewDidLoad() {
        super.viewDidLoad()
       
        
      
        
        
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
          //for setting gradient :
             
                    let gradientLayer: CAGradientLayer = CAGradientLayer()
                                           gradientLayer.frame = view.bounds
                                             let topColor: CGColor = themeColorFaint.cgColor
                                             let middleColor: CGColor = themeColorDark.cgColor
                                            let bottomColor: CGColor = themeColorDark.cgColor
                                             gradientLayer.colors = [topColor, middleColor, bottomColor]
                                                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                               gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                              
                                 self.grad_view.clipsToBounds = true
                                            self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        self.lower_view.clipsToBounds = true
                                                self.lower_view.layer.insertSublayer(gradientLayer, at: 0)
        self.grad_view.applyGradient(colours: [themeColorFaint, themeColorDark])
        self.lower_view.applyGradient(colours: [themeColorFaint, themeColorDark])

         self.Accept_btn.clipsToBounds = true
         
    self.Accept_btn.layer.insertSublayer(gradientLayer, at: 0)
      

        let userData = UserDefaults.standard.value(forKey: "userData") as! NSDictionary
                let name = userData.value(forKey: "name") as! String
        welcome_lbl.text =  "Welcome " + name + " ,Below is the list of deliveries today"
      
        
      //  self.Table_view.allowsSelection = true
        
        
        
        let approval_status = userData.value(forKey: "approval_status") as! String
        
        
        if approval_status == "1"
        {
            self.Table_view.isHidden = false
            notApprovedMsg_lbl.isHidden = true
                        self.welcome_lbl.isHidden = false
            Accept_btn.isHidden = false
        }
        else
        {
            self.Table_view.isHidden = true
            
            notApprovedMsg_lbl.isHidden = false
            self.welcome_lbl.isHidden = true
            Accept_btn.isHidden = true

        }
        
        
       
        
    }
    @objc func timerAction() {
        // locationManager.startUpdatingLocation()
        DriverCurrentloctaion()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        orderidArr.removeAll()
        
               locationManager = CLLocationManager()
               locationManager.delegate = self;
               locationManager.desiredAccuracy = kCLLocationAccuracyBest
               locationManager.requestAlwaysAuthorization()
               locationManager.startUpdatingLocation()
        
        
    // For showing current date & time :
        
          let dateFormatter : DateFormatter = DateFormatter()
              dateFormatter.dateFormat = "dd/MM/YYYY"
              let date = Date()
              let dateString = dateFormatter.string(from: date)
              
              let dateFormatter2 : DateFormatter = DateFormatter()
                     dateFormatter2.dateFormat = "HH:mm a"
              
              let timeString = dateFormatter2.string(from: date)

              
              self.currDate_lbl.text = dateString
              self.currTime_lbl.text = timeString
        
        
    }
    
    //MARK: MAP-VIEW UPDATION METHOD:
      func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
          let locValue:CLLocationCoordinate2D = manager.location!.coordinate
          print("locations = \(locValue.latitude) \(locValue.longitude)")
          currentLoc = manager.location!
        
     

        if loadBool == false
        {
       JobListing()
        }
        else
        {}
        
        DriverCurrentloctaion()
      
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
              
         // locationManager.stopUpdatingLocation()


      }
    
    
    // MARK: - TABLE VIEW METHODS :-
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 310
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HomeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PharmacyDetailCell
        

       let dict = HomeData[indexPath.row] as! NSDictionary

       cell.name_Cell.text = dict.value(forKey: "pharmecy_name") as! String
        cell.place_lbl.text = dict.value(forKey: "pharmecy_address") as! String
        cell.phone_lbl.text = dict.value(forKey: "pharmecy_mobile") as! String
        
        
        let medicine_detail = dict.value(forKey: "medicine_detail") as! NSArray
        let orderid = medicine_detail.value(forKey: "order_id") as! NSArray
        
        let orderidStr = orderid[0]
        cell.totalOrders_lbl.text = orderidStr as! String

        
        let grant_total = dict.value(forKey: "driver_amt") as! String
        
      cell.totalPayment_lbl.text = "$ " + grant_total
        
        let distance = dict.value(forKey: "distance") as! String
        
        cell.distance_lbl.text = distance + " KM"

       cell.selectionStyle = .none
        
     //  cell.selectJob_btn.isSelected = false
        
        cell.selectJob_btn.isUserInteractionEnabled = false
        
        if selectindArr.contains(indexPath.row)
        {
              
            cell.selectJob_btn.isSelected = true
        }
        else
        {
            cell.selectJob_btn.isSelected = false

        }
        
        cell.didselect_btn.tag = indexPath.row
  cell.didselect_btn.addTarget(self, action: #selector(HomeVC.ondidselect_btn(_:)), for: .touchUpInside)
        
        
        
        return cell
    
    }
    @objc func ondidselect_btn(_ sender: UIButton?) {
        
        let indexpth = sender?.tag
        let dict = HomeData[indexpth!] as! NSDictionary
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
           vc.dict = dict
           self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        selectedindex = indexPath.row
        let cell = tableView.cellForRow(at: indexPath) as! PharmacyDetailCell
        
        
        
       

        
        print("selectedindex",selectedindex)
        
       if cell.selectJob_btn.isSelected == true
        
       {
        
//        if selectindArr.count < indexPath.row + 1
//        {
//            orderidArr.remove(at: 0)
//            selectindArr.remove(at: 0)
//
//        }
//        else
//        {
           // orderidArr.remove
        
                //    selectindArr.
        
        
      //  }
        
        let orderid = orderidArr[indexPath.row] as! String
               let selectind = selectindArr[indexPath.row] as! Int
        
        orderidArr.removeAll { $0 == orderid }
        selectindArr.removeAll { $0 == selectind }

     
        print("deorderidArr",orderidArr)
                  print("deselectindArr",selectindArr)
               Table_view.reloadData()
        }
        
        else
       {
        
        
        let dict = HomeData[indexPath.row] as! NSDictionary
        
        let orderid = dict.value(forKey: "id") as! String

        orderidArr.append(orderid)
        selectindArr.append(indexPath.row)
        
        print("orderidArr",orderidArr)
        print("selectindArr",selectindArr)
        Table_view.reloadData()

        }

//       earning
//        wallet
   //  cell.selectJob_btn.isSelected = true
        

    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! PharmacyDetailCell
//        
//        let dict = HomeData[indexPath.row] as! NSDictionary
//        
//        let orderid = dict.value(forKey: "id") as! String
//
//        orderidArr.remove(at: indexPath.row)
//        
//        selectindArr.remove(at: indexPath.row)
//        Table_view.reloadData()
//        
//       
//        
//
//        
//       cell.selectJob_btn.isSelected = false
//    }
    
    
    // MARK: - Webservice Calling

           func  JobListing()
                 {
                    
                    
                      let type = NVActivityIndicatorType.ballClipRotateMultiple
                                             let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                             let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                    self.view.addSubview(activityIndicatorView)
                                   self.view.isUserInteractionEnabled = false


                          activityIndicatorView.startAnimating()
                          var param = [String:Any]()
              
                   param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                    
                    
                param["latitude"] = "\(currentLoc.coordinate.latitude)"
                    param["longitude"] = "\(currentLoc.coordinate.longitude)"

                          WebService().postRequest(methodName: job_list, parameter: param) { (response) in
                              
                             activityIndicatorView.stopAnimating()
                             self.view.isUserInteractionEnabled = true
                              
                              if let newResponse = response as? NSDictionary {
                                  if newResponse.value(forKey: "status") as! Bool  == true {
                                   
                                   let userData = newResponse.value(forKey: "data") as! NSArray
                                     
                                    self.HomeData = userData
                                    
                                    
//                                    for i in 0...self.HomeData.count
//                                    {
//                                        
//                                        self.selectindArr.append(-1)
//                                        
//                                    }
//                                    print("self.selectindArr",self.selectindArr)
//                                    
                                    
                                    
                                    if self.HomeData.count == 0
                                    {
                                        self.Accept_btn.isHidden = true

                                    }
                                    else
                                    {
                                       self.Accept_btn.isHidden = false

                                    }
                                    
                                    
//                                    self.Table_view.allowsSelection = true
//
//                                    self.Table_view.allowsMultipleSelection = true

                                    
                                    self.Table_view.delegate = self
                                    
                                    self.Table_view.dataSource = self
                                    
                                    self.Table_view.reloadData()
                                    
                                    self.loadBool = true
                                   
                                      }
                               else {
                                   
                           self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                 self.Accept_btn.isHidden = true
}
                                  }
                               
                              else {
                                 self.view.showToast(toastMessage: "No Data Found", duration: 1)
                                self.Accept_btn.isHidden = true

                              }
                          }
                      
                 }
    func  DriverCurrentloctaion()
                     {
                        
                        
//                          let type = NVActivityIndicatorType.ballClipRotateMultiple
//                                                 let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
//                                                 let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
//                                        self.view.addSubview(activityIndicatorView)
//                                       self.view.isUserInteractionEnabled = false
                          
                         
                              //activityIndicatorView.startAnimating()
                              var param = [String:Any]()
                  
                       param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                        
                        
                    param["latitude"] = "\(currentLoc.coordinate.latitude)"
                        param["longitude"] = "\(currentLoc.coordinate.longitude)"

                              WebService().postRequest(methodName: current_lat_long, parameter: param) { (response) in
                                  
//                                  activityIndicatorView.stopAnimating()
//                                 self.view.isUserInteractionEnabled = true
                                  
                                  if let newResponse = response as? NSDictionary {
                                      if newResponse.value(forKey: "status") as! Bool  == true {
                                        print("location updated")
                                       
                                      }
                                   else {
                     
    }
                                      }
                                   
                                  else {
                                  

                                  }
                              }
                          
                     }
    
    func  AcceptOrder()
                    {
                         let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                       self.view.addSubview(activityIndicatorView)
                                      self.view.isUserInteractionEnabled = false
                         
                        
                             activityIndicatorView.startAnimating()
                             var param = [String:Any]()
                 
                      param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                       
                        param["order_ids"] = orderidArr.joined(separator: ",")

print("param",param)
                             WebService().postRequest(methodName: accept_order, parameter: param) { (response) in
                                 
                                 activityIndicatorView.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                                 
                                 if let newResponse = response as? NSDictionary {
                                     if newResponse.value(forKey: "status") as! Bool  == true {
                                      
                             self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)
                                        self.selectindArr.removeAll()
                                        self.orderidArr.removeAll()
                                        self.JobListing()
                                      
                                         }
                                  else {
                                      
                              self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                }
                                     }
                                  
                                 else {
                                    self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                 }
                             }
                         
                    }
              
           
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

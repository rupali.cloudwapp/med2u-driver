//
//  PharmacyDetailCell.swift
//  Med2u Driver
//
//  Created by CW-21 on 27/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

class PharmacyDetailCell: UITableViewCell {

    @IBOutlet weak var name_Cell: UILabel!
    
    @IBOutlet weak var place_lbl: UILabel!
    
    
    @IBOutlet weak var phone_lbl: UILabel!
    
    
    @IBOutlet weak var totalOrders_lbl: UILabel!
    
    @IBOutlet weak var totalPayment_lbl: UILabel!
    
    @IBOutlet weak var distance_lbl: UILabel!
    
    
    @IBOutlet weak var selectJob_btn: UIButton!
    
    @IBOutlet weak var didselect_btn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

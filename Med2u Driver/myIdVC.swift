//
//  myIdVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 04/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class myIdVC: UIViewController {
    
    
    @IBOutlet weak var grad_view: UIView!
    
    
    @IBOutlet weak var img_vw: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var idrivethis_lbl: UILabel!
    @IBOutlet weak var signup_buton: DesignableButton!

    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var model: UILabel!
    @IBAction func menu(_ sender: Any) {
        
        
        sideMenuController?.toggle()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        DriverDetails()
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        
        //For setting grdaient :-
                                               
                      self.signup_buton.clipsToBounds = true
                    let gradientLayer: CAGradientLayer = CAGradientLayer()
                        gradientLayer.frame = view.bounds
                             let topColor: CGColor = themeColorFaint.cgColor
                          let middleColor: CGColor = themeColorDark.cgColor
                                  let bottomColor: CGColor = themeColorDark.cgColor
                              gradientLayer.colors = [topColor, middleColor, bottomColor]
                             gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                      gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                    
                        
                        self.grad_view.clipsToBounds = true
                                                                 
                   self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
        
    }
    func  DriverDetails()
                        {
                             let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                    let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                    let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                           self.view.addSubview(activityIndicatorView)
                                          self.view.isUserInteractionEnabled = false
                             
                            
                                 activityIndicatorView.startAnimating()
                                 var param = [String:Any]()
                     
                          param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                           
                           
                                 WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                                     
                                     activityIndicatorView.stopAnimating()
                                    self.view.isUserInteractionEnabled = true
                                     
                                     if let newResponse = response as? NSDictionary {
                                         if newResponse.value(forKey: "status") as! Bool  == true {
                                          
                                          let userData = newResponse.value(forKey: "data") as! NSDictionary
                                            
                                          print("userData",userData)
                                           
                                            
                                   
                self.model.text = userData.value(forKey: "plate_no") as! String
    self.make.text = userData.value(forKey: "plate_no") as! String
      self.name.text = userData.value(forKey: "name") as! String
             let image = userData.value(forKey: "image") as! String
                                            
       let datee =  userData.value(forKey: "created_on") as! String
                                            
          let dateform = DateFormatter()
      dateform.dateFormat = "yyyy-MM-dd HH:mm:ss"
      let dateget = dateform.date(from: datee)
                                            
     print("dateget",dateget)
                                            
              let dateform2 = DateFormatter()
              dateform2.dateFormat = "dd/MM/yyyy"
          let dateset = dateform2.string(from: dateget!) as! String
                                            

        
            self.date.text = dateset
                                                                                  

     self.img_vw.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
                                            
                                        
                                            
                                             }
                                      else {
                                          
                                  self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                    }
                                         }
                                      
                                     else {
                                        self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                     }
                                 }
                             
                        }
                  


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

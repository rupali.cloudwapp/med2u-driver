//
//  signatureVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 04/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import SignaturePad
import NVActivityIndicatorView

class signatureVC: UIViewController,SignaturePadDelegate {
   
    @IBOutlet weak var cust_image: UIImageView!
    
    @IBOutlet weak var deriver_img: UIImageView!
    var orderideStr = String()

    @IBOutlet weak var signatureHeading_lbl: UILabel!
    
    @IBOutlet weak var siganture_pad: SignaturePad!
    @IBOutlet weak var grad_view: UIView!
    
    @IBOutlet weak var next_btn: UIButton!
    
    
    @IBOutlet weak var save_btn: UIButton!
    
    
    @IBOutlet weak var popup_view: UIView!
    @IBAction func menu_act(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func ok_act(_ sender: Any) {
        
        self.popup_view.isHidden = true
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func cross_act(_ sender: Any) {
        
        custBool = false
        driverBool = false
        
        custsavedBool = false
        driversavedBool = false

         
        siganture_pad.clear()

    }
    
    
    @IBAction func save_act(_ sender: Any) {
      
          if self.next_btn.titleLabel?.text == "Next"
                               {
                                
                             
        custsavedBool = true
                               }
                               else
                               {
                                
        driversavedBool = true
                               }
         self.view.showToast(toastMessage: "Saved", duration: 1)
      //self.next_btn.setTitle("DELIVER IT", for: .normal)

    }
    
    @IBAction func next_act(_ sender: Any) {
        
        if self.next_btn.titleLabel?.text == "Next"
        {
            if custsavedBool == false
                       {
             self.view.showToast(toastMessage: "Customer Signature Required!!", duration: 1)
                       }
            else
            {
            
            siganture_pad.clear()
            signatureHeading_lbl.text = "Driver Signature"
            self.next_btn.setTitle("DELIVER IT", for: .normal)
            }

        }
        else
        {
         
            if driversavedBool == false
            {
                self.view.showToast(toastMessage: "Driver Signature Required!!", duration: 1)

            }
            else
            {
                ReturnServicewithSignature()
            }
            
            
        }
        

    }
    
    
    var customerSignImg = UIImage()
    var driverSignImg = UIImage()
    
    var custBool = false
    var driverBool = false
    
    
    var custsavedBool = false
    var driversavedBool = false

var userloginData = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        userloginData = UserDefaults.standard.value(forKey: "userData") as! NSDictionary
        print("userloginData",userloginData)
        

        signatureHeading_lbl.text = "Customer Signature"

        self.next_btn.setTitle("Next", for: .normal)

        let gradientLayer: CAGradientLayer = CAGradientLayer()
                     gradientLayer.frame = view.bounds
                       let topColor: CGColor = themeColorFaint.cgColor
                       let middleColor: CGColor = themeColorDark.cgColor
                      let bottomColor: CGColor = themeColorDark.cgColor
                       gradientLayer.colors = [topColor, middleColor, bottomColor]
                          gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                         gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
           self.grad_view.clipsToBounds = true
                      self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
            self.save_btn.layer.insertSublayer(gradientLayer, at: 0)
        
        
        self.next_btn.clipsToBounds = true
        
        self.next_btn.applyGradient(colours: [themeColorFaint, themeColorDark])
        self.save_btn.layer.insertSublayer(gradientLayer, at: 0)

        self.siganture_pad.delegate = self


    }
    
    func didStart() {
            if let signature = siganture_pad.getSignature() {
print("signaturestart",signature)
                
                
        }
       }
       
       func didFinish() {
            if let signature = siganture_pad.getSignature() {
           print("signatureend",signature)
                
                
                
                if self.next_btn.titleLabel?.text == "Next"
                       {
                        
                        self.cust_image.image = signature
                        self.customerSignImg = signature
custBool = true
                       }
                       else
                       {
                        self.deriver_img.image = signature

                           self.driverSignImg = signature
driverBool = true
                       }
                           
                           
                   }
       }
    
  
    func  ReturnServicewithSignature()
       {
           
        let type = NVActivityIndicatorType.ballClipRotateMultiple
                     let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                     let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
            self.view.addSubview(activityIndicatorView)
           self.view.isUserInteractionEnabled = false
               activityIndicatorView.startAnimating()
        
        
                var param = [String:Any]()
     

      param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                          
             param["delivery_receiver_name"] = userloginData.value(forKey: "name") as! String
                            
                         param["order_id"] = orderideStr
                         param["order_type"] = "delivered"
                         param["return_pharmacy_reason"] = ""
                          
        param["delivery_receiver_sign"] = UIImageJPEGRepresentation(self.cust_image.image!, 1)
        param["delivery_driver_sign"] = UIImageJPEGRepresentation(self.deriver_img.image!, 1)

      //   pink_green_slip:xxxxx (image)
        
        print("param",param)

      WebService().uploadImagesHandler(methodName: delivered_and_return , parameter: param) { (response) in
                                 
                                activityIndicatorView.stopAnimating()
                            self.view.isUserInteractionEnabled = true


                                     if let newResponse = response as? NSDictionary {
                            if newResponse.value(forKey: "status") as! Bool  == true {
                
                                self.siganture_pad.clear()
                                self.popup_view.isHidden = false
                                
                                }
                                        
                                        else
                                        {

                                            self.view.showToast(toastMessage: "Failed to submit", duration: 1)

                                                    }
                                   
                                }
                                 
                                 else {
                                       self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                       
                                 }
                             }
                             }
     
           
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

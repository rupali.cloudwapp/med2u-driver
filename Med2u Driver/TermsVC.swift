//
//  TermsVC.swift
//  MED 2U
//
//  Created by CW-21 on 20/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import youtube_ios_player_helper

class TermsVC: UIViewController {
    
    
    
    @IBOutlet weak var youtube_height: NSLayoutConstraint!
    @IBOutlet weak var youtube_view: YTPlayerView!
    
    
    @IBOutlet weak var heading_lbl: UILabel!
    @IBOutlet weak var grad_view: UIView!
    
    @IBOutlet weak var web_view: UIWebView!
    
    
    @IBAction func back_act(_ sender: Any) {
        
        sideMenuController?.toggle()
        
       // self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.bool(forKey: "terms") == true
                           {
                              heading_lbl.text = "Terms & Conditions"
                            youtube_height.constant = 0

                           }
                           else
                           {
                               heading_lbl.text = "Instructions"
                            youtube_height.constant = 180

                           }
        
        
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        //for setting gradient :
        
                   let gradientLayer: CAGradientLayer = CAGradientLayer()
                     gradientLayer.frame = view.bounds
                       let topColor: CGColor = themeColorFaint.cgColor
                       let middleColor: CGColor = themeColorDark.cgColor
                      let bottomColor: CGColor = themeColorDark.cgColor
                       gradientLayer.colors = [topColor, middleColor, bottomColor]
                          gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                         gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
           self.grad_view.clipsToBounds = true
                      self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
        
        
ServiceTerms()
        // Do any additional setup after loading the view.
    }
    func ServiceTerms()
                 {
                     let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                           let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                           let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                                  self.view.addSubview(activityIndicatorView)
                                                 self.view.isUserInteractionEnabled = false

                     activityIndicatorView.startAnimating()
                     var param = [String:Any]()
                     
                     
                     print("param",param)
                    
                    var method = String()

                    if UserDefaults.standard.bool(forKey: "terms") == true
                    {
                       method = terms_driver
                        
                    }
                    else
                    {
                        method = instruction_driver

                    }
                    
                    
                     
                     WebService().postRequest(methodName: method , parameter: param) { (response) in
                         print("response",response)
                         
                         activityIndicatorView.stopAnimating()
                         self.view.isUserInteractionEnabled = true

                         if let newResponse = response as? NSDictionary {
                             if newResponse.value(forKey: "status") as! Bool  == true {
                                 
                                 let data = newResponse.value(forKey: "data") as! NSDictionary
                                 
                               let descr = data.value(forKey: "description") as! String
                                 let descriptionStr = descr
                                 let descFinal = descriptionStr.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "")
                                 let descFinal2 = descFinal.replacingOccurrences(of: "\n", with: " ").replacingOccurrences(of: "\"", with: "")
                               //  let content = "<html><body><p><font size=30> \(descFinal2)</font></p></body></html>"
                                 
                                // let test = String(content.filter { !" \n\t\r".contains($0) })
                                 
         //                        self.web_view.loadHTMLString(content, baseURL: nil)
         //                        print("content",content)
                                // let descWithouthtml = descr.withoutHtml as! String
                                 
                            let content = "<html><body><p><font size=4> \(descriptionStr)</font></p></body></html>"

                                 self.web_view.loadHTMLString(content, baseURL: nil)
                                
                                if UserDefaults.standard.bool(forKey: "terms") == true
                                {
                                    
                                }
                                else
                                {
                                   let  urlvideo = data.value(forKey: "video_link") as! String

                                                                   let urll =  urlvideo
                                                                           
                                                                           print("urll",urll)
                                                                           
                                                                           if urll == ""
                                                                           {
                                                                             
                                                                          self.view.showToast(toastMessage: "No video in this location", duration: 1)
                                                                               self.youtube_height.constant = 0

                                                                           }
                                                                           else
                                                                           {
                                                                           
                                                                           
                                                                               let url = urll.components(separatedBy: "=")
                                                                               let url1 = url[1] as! String
                                                                               self.youtube_view.load(withVideoId:url1 )
                                                                               
                                                                               self.youtube_height.constant = 180

                                                                           }
                                }
                                 
                                 
                             }
                             else {
                                 self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                             }
                         }
                         else if let newMsg = response as? String{
                             self.view.showToast(toastMessage: newMsg, duration: 1)

                         }
                         else {
                             
                             self.view.showToast(toastMessage: "No Data Found", duration: 1)

                         }
                     }
                     
                 }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

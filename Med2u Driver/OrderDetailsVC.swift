//
//  OrderDetailsVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 03/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import MapKit

class OrderDetailsVC: UIViewController,MKMapViewDelegate {
    
    var dict = NSDictionary()
    var heading = String()

    
    @IBOutlet weak var orderid_lbl: UILabel!
    
    @IBOutlet weak var customername_lbl: UILabel!
    
    @IBOutlet weak var custmobile_lbl: UILabel!
    
    @IBOutlet weak var custpriaddress_lbl: UILabel!
    
    @IBOutlet weak var custcity_lbl: UILabel!
    
    
    @IBOutlet weak var custstate_lbl: UILabel!
    
    
    @IBOutlet weak var custpostcode_lbl: UILabel!
    
    @IBOutlet weak var custsecaddress_lbl: UILabel!
    
    @IBOutlet weak var custdistance_lbl: UILabel!
    
    
    @IBOutlet weak var pharmacyorderid_bll: UILabel!
    
    
    @IBOutlet weak var phramcyname_lbl: UILabel!
    
    @IBOutlet weak var pharmacynumber_lbl: UILabel!
    @IBOutlet weak var pharmcyaddress_lbl: UILabel!
    
    
    @IBOutlet weak var button_stack: UIStackView!
    
    
    
    @IBOutlet weak var map_view: MKMapView!
    
    
    @IBOutlet weak var grad_view: UIView!

        
    @IBOutlet weak var return_btn: UIButton!
    @IBOutlet weak var deliver_btn: UIButton!
    //MARK : IBACTIONS:
       
    @IBAction func deliver_act(_ sender: Any) {
        
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "signatureVC") as! signatureVC
                vc.orderideStr = dict.value(forKey: "id") as! String

                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func return_act(_ sender: Any) {
        
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReasonVC") as! ReasonVC
        
        vc.camefrom = "details"

        
        vc.orderideStr = dict.value(forKey: "id") as! String
        
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    @IBAction func menu_act(_ sender: Any) {
           
        self.navigationController?.popViewController(animated: true)
        
       }
     
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        
        
        

            self.map_view.delegate = self
        let lat = dict.value(forKey: "latitude") as! String
               let long = dict.value(forKey: "longitude") as! String
               let latitude =  Double(lat)
                                  let longitude = Double(long)
              
               
               self.map_view.isZoomEnabled = true
               
             let center = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
               let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
               self.map_view.setRegion(region, animated: true)

               let annotation = MKPointAnnotation()
               annotation.coordinate = center
               annotation.title = dict.value(forKey: "address1") as! String
            //   annotation.subtitle = "current location"
               self.map_view.addAnnotation(annotation)

                            let gradientLayer: CAGradientLayer = CAGradientLayer()
                              gradientLayer.frame = view.bounds
                                let topColor: CGColor = themeColorFaint.cgColor
                                let middleColor: CGColor = themeColorDark.cgColor
                               let bottomColor: CGColor = themeColorDark.cgColor
                                gradientLayer.colors = [topColor, middleColor, bottomColor]
                                   gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                  gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                 
                    self.grad_view.clipsToBounds = true
                               self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
    
       
        orderid_lbl.text = dict.value(forKey: "id") as! String
        customername_lbl.text = dict.value(forKey: "name") as! String
        custmobile_lbl.text = dict.value(forKey: "mobile") as! String
        custpriaddress_lbl.text = dict.value(forKey: "address1") as! String
        custcity_lbl.text = dict.value(forKey: "city") as! String
        custstate_lbl.text = dict.value(forKey: "state") as! String
        custpostcode_lbl.text = dict.value(forKey: "postcode") as! String
        custsecaddress_lbl.text = dict.value(forKey: "secondary_address") as! String
        custdistance_lbl.text = dict.value(forKey: "distance") as! String + " KM"

        pharmacyorderid_bll.text = dict.value(forKey: "pharmecy_id") as! String
              phramcyname_lbl.text = dict.value(forKey: "pharmecy_name") as! String
              pharmacynumber_lbl.text = dict.value(forKey: "pharmecy_mobile") as! String
        pharmcyaddress_lbl.text = dict.value(forKey: "pharmecy_address") as! String

    
        
        heading = UserDefaults.standard.value(forKey: "headingName") as! String
        
        
        self.return_btn.clipsToBounds = true
        self.deliver_btn.clipsToBounds = true
        
        
         if heading == "In Transit Orders"
         {
          self.button_stack.isHidden = false
          self.return_btn.isHidden = false
          self.deliver_btn.isHidden = false
           
       
        }
        else
         {
          self.button_stack.isHidden = true
                     self.return_btn.isHidden = true
                     self.deliver_btn.isHidden = true
        }
        

        
             
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

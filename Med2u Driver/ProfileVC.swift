//
//  ProfileVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 27/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import MKDropdownMenu
import NVActivityIndicatorView
import AlamofireImage

@available(iOS 13.0, *)
class ProfileVC: UIViewController {
    
    
    @IBOutlet weak var noregistration_lbl: UILabel!
    @IBOutlet weak var nopolice_lbl: UILabel!
    @IBOutlet weak var nolicence_lbl: UILabel!
    @IBAction func ok_act(_ sender: Any) {
          self.Ppopup_vc.isHidden = true

          
      }
    @IBAction func EDIT_ACT(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
           
           
               self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    @IBOutlet weak var Ppopup_vc: UIView!
    
    
    //MARK : IBOUTLETS:
    
    @IBOutlet weak var name_tf: UITextField!
    
    @IBOutlet weak var email_tf: UITextField!
    
    
    @IBOutlet weak var mobile_tf: UITextField!
    
      
      @IBOutlet weak var addressLine1_tf: UITextField!
      @IBOutlet weak var state_tf: UITextField!
      @IBOutlet weak var postcode_tf: UITextField!
      @IBOutlet weak var suburb_tf: UITextField!
      @IBOutlet weak var vehicletype_tf: UITextField!
      @IBOutlet weak var vehiclemake_tf: UITextField!
      @IBOutlet weak var vehiclemodel_tf: UITextField!
      @IBOutlet weak var bankAcc_tf: UITextField!
      @IBOutlet weak var abn_tf: UITextField!
      @IBOutlet weak var thirdparty_tf: UITextField!
      @IBOutlet weak var ctpEnsured_tf: UITextField!
        
      @IBOutlet weak var vehicleType_dropdown: MKDropdownMenu!
      
      @IBOutlet weak var thirdparty_dropdown: MKDropdownMenu!
      @IBOutlet weak var CTP_dropdown: MKDropdownMenu!
     
      @IBOutlet weak var name_lbl: UILabel!
      @IBOutlet weak var signup_buton: DesignableButton!
      @IBOutlet weak var pro_img: UIImageView!
      @IBOutlet weak var gradient_view: UIView!
      @IBOutlet weak var vehiclemake_lbl: UILabel!
      @IBOutlet weak var vehiclemodel_lbl: UILabel!
    
      @IBOutlet weak var ensure_lbl: UILabel!
      
      
      var appDele = UIApplication.shared.delegate as! AppDelegate

      @IBOutlet weak var scroll_view: UIScrollView!
      
    @IBOutlet weak var police_img: UIImageView!
    @IBOutlet weak var license_img: UIImageView!
    
    @IBOutlet weak var registartion_img: UIImageView!
    
    
    
    //MARK : IBACTIONS:
      
    
      
     
      @IBAction func address1_act(_ sender: Any) {
          
       
        
      }
      
    @IBAction func pro_act(_ sender: Any) {
           
          
       }
       
       @IBAction func BACK(_ sender: Any) {
          
        sideMenuController?.toggle()
        
            }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        name_lbl.isHidden = true
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        DriverDetails()

        
        addressLine1_tf.isUserInteractionEnabled = false
        state_tf.isUserInteractionEnabled = false
        postcode_tf.isUserInteractionEnabled = false
        vehicletype_tf.isUserInteractionEnabled = false
        vehiclemake_tf.isUserInteractionEnabled = false
        vehiclemodel_tf.isUserInteractionEnabled = false
        bankAcc_tf.isUserInteractionEnabled = false
        abn_tf.isUserInteractionEnabled = false
        thirdparty_tf.isUserInteractionEnabled = false
        ctpEnsured_tf.isUserInteractionEnabled = false
        vehicleType_dropdown.isUserInteractionEnabled = false
        thirdparty_dropdown.isUserInteractionEnabled = false
        CTP_dropdown.isUserInteractionEnabled = false
        name_tf.isUserInteractionEnabled = false
        email_tf.isUserInteractionEnabled = false
        mobile_tf.isUserInteractionEnabled = false

        self.signup_buton.isHidden = false
        
        
            
         //For setting grdaient :-
                                          
                 self.signup_buton.clipsToBounds = true
               let gradientLayer: CAGradientLayer = CAGradientLayer()
                   gradientLayer.frame = view.bounds
                        let topColor: CGColor = themeColorFaint.cgColor
                     let middleColor: CGColor = themeColorDark.cgColor
                             let bottomColor: CGColor = themeColorDark.cgColor
                         gradientLayer.colors = [topColor, middleColor, bottomColor]
                        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                 gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
               
                   
                   self.gradient_view.clipsToBounds = true
                                                            
              self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
        

        
    }
    
    func  DriverDetails()
                    {
                         let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                       self.view.addSubview(activityIndicatorView)
                                      self.view.isUserInteractionEnabled = false
                         
                        
                             activityIndicatorView.startAnimating()
                             var param = [String:Any]()
                 
                      param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                       
                       
                             WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                                 
                                 activityIndicatorView.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                                 
                                 if let newResponse = response as? NSDictionary {
                                     if newResponse.value(forKey: "status") as! Bool  == true {
                                      
                                      let userData = newResponse.value(forKey: "data") as! NSDictionary
                                        
                                      print("userData",userData)
                                        
                                        self.addressLine1_tf.text = userData.value(forKey: "address1") as! String
                                        
                                      self.state_tf.text = userData.value(forKey: "state") as! String
                                        
                                        self.suburb_tf.text = userData.value(forKey: "suburb") as! String
                                        
                                        self.postcode_tf.text = userData.value(forKey: "postcode") as! String
                                        
                                        
                           let vehicle_model = userData.value(forKey: "vehicle_model") as! String
                               if vehicle_model == "Car"
                                          {
                                              
                                            self.vehiclemake_lbl.text = "Vehicle Make/Year"
                                            self.vehiclemodel_lbl.text = "Vehicle Model"
                                              
                                              
                                            self.vehiclemake_tf.placeholder = "Vehicle Make/Year"
                                            self.vehiclemodel_tf.placeholder = "Vehicle Model"
                                              
                                          }
                                        
                                        else  if vehicle_model == "Motorcycle"
                                          {
                                              
                                            self.vehiclemake_lbl.text = "Motorcycle Make/Year"
                                            self.vehiclemodel_lbl.text = "Motorcycle Model"
                                              
                                              
                                            self.vehiclemake_tf.placeholder = "Motorcycle Make/Year"
                                            self.vehiclemodel_tf.placeholder = "Motorcycle Model"
                                              
                                          }
                                          else
                                          {
                                            self.vehiclemake_lbl.text = "License Number"
                                            self.vehiclemodel_lbl.text = "BSB"
                                                             
                                            self.vehiclemake_tf.placeholder = "License Number"
                                            self.vehiclemodel_tf.placeholder = "BSB"
                                              
                                          }
                                        
                                        
                                        
                                        self.vehicletype_tf.text = userData.value(forKey: "vehicle_model") as! String
self.vehiclemake_tf.text = userData.value(forKey: "vehicle_make") as! String
                                        self.vehiclemodel_tf.text = userData.value(forKey: "license_no") as! String
                                        
                                        self.bankAcc_tf.text = userData.value(forKey: "bank_account_no") as! String
                                        self.abn_tf.text = userData.value(forKey: "abn") as! String
                                        self.thirdparty_tf.text = userData.value(forKey: "hold_third_party") as! String

                                     self.ctpEnsured_tf.text = userData.value(forKey: "insure_your_car") as! String
                                        self.name_tf.text = userData.value(forKey: "name") as! String
                                        self.email_tf.text = userData.value(forKey: "email") as! String
                                        self.mobile_tf.text = userData.value(forKey: "mobile") as! String
                                        
                                        
                                        let certificate_of_registration = userData.value(forKey: "certificate_of_registration") as! String
let police_check_certificate = userData.value(forKey: "police_check_certificate") as! String
                                      
                                        let copy_driving_record = userData.value(forKey: "copy_driving_record") as! String
                                        
         if certificate_of_registration == ""
         {
            self.noregistration_lbl.text = "No copy of Registration uploaded"
            
                                        }
        else
         {
            self.noregistration_lbl.text = ""

                                        }
    
                                        if copy_driving_record == ""
                                                {
                                                    self.nolicence_lbl.text = "No copy of Driving Licence uploaded"
                                                   
                                                                               }
                                               else
                                                {
                                                    self.nolicence_lbl.text = ""
                                                                               }
                                        if copy_driving_record == ""
                                                                                       {
                                                                                           self.nolicence_lbl.text = "No copy of Licence uploaded"
                                                                                          
                                                                                                                      }
                                                                                      else
                                                                                       {
                                                                                           self.nolicence_lbl.text = ""
                                                                                                                      }
                                        if police_check_certificate == ""
                                         {
                                             self.nopolice_lbl.text = "No copy of Police Check Certificate uploaded"
                                            
                                                                        }
                                        else
                                         {
                                             self.nopolice_lbl.text = ""
                                                                        }
                                        
                                        
                                        
                                        let image = userData.value(forKey: "image") as! String
                                                                              

                                     
                                        self.pro_img.af_setImage(withURL: URL(string: "\(img_Url)\(image)")!)
                                        
                                     self.license_img.af_setImage(withURL: URL(string: "\(img_Url)\(copy_driving_record)")!)
                                        
 self.registartion_img.af_setImage(withURL: URL(string: "\(img_Url)\(certificate_of_registration)")!)
                                        
  self.police_img.af_setImage(withURL: URL(string: "\(img_Url)\(police_check_certificate)")!)
                                        
                                        
                                         }
                                  else {
                                      
                              self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                }
                                     }
                                  
                                 else {
                                    self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                 }
                             }
                         
                    }
              

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

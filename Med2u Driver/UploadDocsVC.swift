//
//  UploadDocsVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 23/05/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

@available(iOS 13.0, *)
class UploadDocsVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var gradient_view: UIView!

    @IBOutlet weak var addressLine1_tf: UITextField!
       @IBOutlet weak var state_tf: UITextField!
       @IBOutlet weak var postcode_tf: UITextField!
       @IBOutlet weak var suburb_tf: UITextField!
       @IBOutlet weak var vehicletype_tf: UITextField!
       @IBOutlet weak var vehiclemake_tf: UITextField!
       @IBOutlet weak var vehiclemodel_tf: UITextField!
       @IBOutlet weak var bankAcc_tf: UITextField!
       @IBOutlet weak var abn_tf: UITextField!
       @IBOutlet weak var thirdparty_tf: UITextField!
       @IBOutlet weak var ctpEnsured_tf: UITextField!
    
    var topassLat = String()
    var topassLong = String()
    
    var licenseBool = Bool()
    var policeBool = Bool()
    var regisBool = Bool()

    
   var addressLine1 = String()
    
    var state = String()
    var postcode = String()
    var suburb = String()
    var vehicletyp = String()
    var vehiclemake = String()
    var vehiclemodel = String()
    var bankAcc = String()
    var thirdparty = String()
    var ctpEnsured = String()
    var city = String()

    var imgStr = String()
    var emailStr = String()
    var mobileStr = String()
    var PwStr = String()
    var nameStr = String()
            
    var pickedImagee = UIImage()

    let picker = UIImagePickerController()

    
    
    @IBAction func ok_act(_ sender: Any) {
           self.Ppopup_vc.isHidden = true
      if #available(iOS 13.0, *) {
                                      let scene = UIApplication.shared.connectedScenes.first
                          if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                              
                              sd.setHomeRootController()
                              
                                      }
                                       
                           }
                                       
                           else
                           {

                              self.appDele.setHomeRootController()

                          }

           
       }
       @IBOutlet weak var Ppopup_vc: UIView!
    
    
    var licenceAttached = false
       var policeAttached = false
    var registartionAttached = false

    
    @IBOutlet weak var signup_buton: DesignableButton!

   
    @IBOutlet weak var vw3: UIView!
    @IBOutlet weak var vw1: UIView!
    
    @IBOutlet weak var vw2: UIView!
    @IBAction func license_act(_ sender: Any) {
        
        licenseBool = true
        policeBool = false
        regisBool = false
        
        
       

      openFileAttachment ()
           
       }
    
    @IBAction func policeChk_act(_ sender: Any) {
        
        licenseBool = false
             policeBool = true
             regisBool = false
openFileAttachment ()
              
          }
    @IBAction func Registration_act(_ sender: Any) {
        
        licenseBool = false
                    policeBool = false
                    regisBool = true
        

       openFileAttachment ()
          }
    
    @IBOutlet weak var license_img: UIImageView!
    @IBOutlet weak var policeChk_img: UIImageView!
    @IBOutlet weak var registartiomn_img: UIImageView!

    
    
    @IBAction func BACK(_ sender: Any) {
    
         self.navigationController?.popViewController(animated: true)
     }
    
    
    
    
    @IBAction func signup_act(_ sender: Any) {
        
        
        if licenceAttached == false
        {
            self.view.showToast(toastMessage: "Driving licence required!!", duration: 1)
        }
        else if policeAttached == false
        {
            self.view.showToast(toastMessage: "Police check cerificate required!!", duration: 1)

        }
        else if registartionAttached == false
        {
            self.view.showToast(toastMessage: "Certificate of Registration required!!", duration: 1)

        }
        else
        {
            signupDriver()
                   
        }
        

    
        
    }
    
    var appDele = UIApplication.shared.delegate as! AppDelegate

    

    override func viewDidLoad() {
        super.viewDidLoad()

        picker.delegate = self
        
        
      if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        
        
        
        
        //For setting grdaient :-
                                  
                                         self.signup_buton.clipsToBounds = true
                                         let gradientLayer: CAGradientLayer = CAGradientLayer()
                                         gradientLayer.frame = view.bounds
                                         let topColor: CGColor = themeColorFaint.cgColor
                                         let middleColor: CGColor = themeColorDark.cgColor
                                       let bottomColor: CGColor = themeColorDark.cgColor
                                         gradientLayer.colors = [topColor, middleColor, bottomColor]
                                         gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                         gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        //   self.signup_buton.layer.insertSublayer(gradientLayer, at: 0))
           
           self.signup_buton.applyGradient(colours: [themeColorFaint, themeColorDark])
           
           
           self.gradient_view.clipsToBounds = true
             self.gradient_view.layer.insertSublayer(gradientLayer, at: 0)
        self.vw1.clipsToBounds = true
         self.vw2.clipsToBounds = true
         self.vw3.clipsToBounds = true
      
        self.vw1.applyGradient(colours: [themeColorFaint, themeColorDark])
              self.vw2.applyGradient(colours: [themeColorFaint, themeColorDark])
              self.vw3.applyGradient(colours: [themeColorFaint, themeColorDark])
        
    }
    
    //METHOD FOR IMAGE PICKING:
       
     
       func openFileAttachment () {
           self.view.endEditing(true)
           let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
           actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
               
               self.openCamera()
           }))
           actionSheet.addAction(UIAlertAction(title:"Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
               
               self.openGallery()
           }))
           
           actionSheet.addAction(UIAlertAction(title:"Cancel", style: UIAlertAction.Style.cancel, handler: nil))
           self.present(actionSheet, animated: true, completion: nil)
       }
       
       
       func openCamera() {
           if UIImagePickerController.isSourceTypeAvailable(.camera){
               picker.allowsEditing = false
               picker.sourceType = .camera
               picker.cameraCaptureMode = .photo
               present(picker, animated: true, completion: nil)
           }
           else{
               let alert = UIAlertController(title: "Alert", message: "No Camera found in your Device", preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
               self.present(alert, animated: true, completion: nil)
           }
       }
       
       func openGallery() {
           picker.allowsEditing = true
           picker.sourceType = .photoLibrary
           // present(picker, animated: true, completion: nil)
           
           self.present(picker, animated: true, completion: nil)
       }
       // MARK: - ImagePicker Method
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
           if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            
          if   licenseBool == true
          {
            license_img.image = pickedImage
            licenceAttached = true

            
            }
           else if   policeBool == true
            {
              policeChk_img.image = pickedImage
              policeAttached = true

              }
            else
          {
            registartiomn_img.image = pickedImage
            registartionAttached = true


            }
            
           }
           
          picker.dismiss(animated: true, completion: nil)
       }
       
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           dismiss(animated: true, completion: nil)
       }
    
     func  signupDriver()
      {
          let type = NVActivityIndicatorType.ballClipRotateMultiple
                    let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                    let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
           self.view.addSubview(activityIndicatorView)
          self.view.isUserInteractionEnabled = false
              activityIndicatorView.startAnimating()
       
       let fcmtoken = UserDefaults.standard.value(forKey: "self.fcmToken") as! String

               var param = [String:Any]()
    

       param["name"] = self.nameStr
        param["email"] = emailStr
          param["password"] = PwStr
          param["mobile"] = mobileStr
        param["suburb"] = suburb
                 param["city"] = suburb
                 param["state"] = state
                 param["postcode"] = postcode
      
          param["address"] = addressLine1
         
          param["latitude"] = topassLat
          param["longitude"] = topassLong
   
        param["vehicle_make"] = vehiclemake
        param["vehicle_model"] = vehiclemodel
        param["plate_no"] = vehiclemake
        param["license_no"] = vehiclemodel
        param["bank_account_no"] = bankAcc
        
        param["abn"] = topassLong
        param["bsb"] = topassLong

        param["hold_third_party"] = thirdparty
        param["insure_your_car"] = ctpEnsured

     
          param["fcm_token"] = fcmtoken
          param["device_type"] = "IOS"
       param["device_id"] =  UserDefaults.standard.value(forKey: "decviceId") as! String
       
        param["image"] = UIImageJPEGRepresentation(pickedImagee, 1)!
        param["copy_driving_record"] = UIImageJPEGRepresentation(license_img.image!, 1)!
        param["police_check_certificate"] = UIImageJPEGRepresentation(policeChk_img.image!, 1)!
        param["certificate_of_registration"] = UIImageJPEGRepresentation(registartiomn_img.image!, 1)!


     //   pink_green_slip:xxxxx (image)

     WebService().uploadImagesHandler(methodName: RegistrationService , parameter: param) { (response) in
                                
                               activityIndicatorView.stopAnimating()
                           self.view.isUserInteractionEnabled = true


                                    if let newResponse = response as? NSDictionary {
                           if newResponse.value(forKey: "status") as! Bool  == true {
               if let userData = newResponse.value(forKey: "data") as? NSDictionary {
                                           
                        print("userData",userData)
                                                                     
                       let userid = userData.value(forKey: "id") as! String
                                                              
                                                                     
                     UserDefaults.standard.set(userid, forKey: "userIdLogin")
                                                                 UserDefaults.standard.set(userData, forKey: "userData")
                
                
                
                self.Ppopup_vc.isHidden = false
                
                                                                                                                                                            
                   /* if #available(iOS 13.0, *) {
                               let scene = UIApplication.shared.connectedScenes.first
                   if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
                       
                       sd.setHomeRootController()
                       
                               }
                                
                    }
                                
                    else
                    {

                       self.appDele.setHomeRootController()

                   }*/

                   
                               }
                                    }
                                       else
                           {

                               self.view.showToast(toastMessage: "Failed to Registered", duration: 1)

                                       }
                                    
                               }
                                
                                else {
                                      self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                      
                                }
                            }
                            }
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

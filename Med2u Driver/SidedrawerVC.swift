//
//  SidedrawerVC.swift
//  Dennish
//
//  Created by CW on 08/02/19.
//  Copyright © 2019 CW. All rights reserved.
//

import UIKit



@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SidedrawerVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
var menuArray = NSArray()
var imgArray = NSArray()
 var selectindex = -1
  
    @IBAction func profile_act(_ sender: Any) {
       
    }
    
    @IBOutlet weak var side_table: UITableView!
    
    
    //for PROFILE
    var parasedit = [String : String]()
    
    var datadictionary = NSDictionary()
    var indatadictionary = NSDictionary()
    var statustring = Bool()
    var text = NSString()
    var idtsrng = NSString()
    var emailstring = NSString()
    var dataArray = NSDictionary()
    let kPUserDefault = UserDefaults.standard
    var paraLg = [String : String]()
    
    let appDele = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        
        
        
        
        
        let userData = UserDefaults.standard.value(forKey: "userData") as! NSDictionary

        let approval_status = userData.value(forKey: "approval_status") as! String
        
        
        if approval_status == "1"
        {
            menuArray = ["Home","My Profile","My Id","Earnings","Order Accepted List","Order InTransit List","Order Delivered List","Order Return List","Terms & Conditions","Instructions","Logout"]
            
            
             imgArray = ["home icon","profile icon","my order icon","payment icon","terms icon","terms icon","terms icon","terms icon","terms icon","terms icon","logout icon"]
        }
        else
        {
          menuArray = ["Home","Terms & Conditions","Logout"]
                        imgArray = ["home icon","profile icon","my cart icon","my order icon","payment icon","terms icon","logout icon"]

        }
        
            


        side_table.delegate = self
        side_table.dataSource = self

        
      //  ViewProfile()
        

        
       // self.imgArray = ["Home","logout"]
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
//   ViewProfile()
    //  name_lbl.text = UserDefaults.standard.value(forKey: "Loginname") as! String
        
   //  ViewProfile()
      
      NotificationCenter.default.addObserver(self, selector: #selector(methodprofile), name: Notification.Name("ViewProfile"), object: nil)
    }
    @objc func methodprofile(notification: NSNotification) {
      // ViewProfile()
        
        // Take Action on Notification
    }
  
    
    
    @IBOutlet weak var logo: UIImageView!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
   
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = side_table.dequeueReusableCell(withIdentifier: "cell") as! SideCellTableViewCell
       cell.selectionStyle = .none
        cell.lbl.text = menuArray[indexPath.row] as? String
        cell.lbl.textColor  = UIColor.darkGray
     cell.img.image = UIImage(named: imgArray[indexPath.row] as! String)
        
       
        if selectindex == indexPath.row {
            cell.backgroundColor = themeColorFaint.withAlphaComponent(0.2)
        } else {
            cell.backgroundColor = UIColor.clear
        }
        
        
//        let bgColorView = UIView()
//        bgColorView.backgroundColor = themeColorFaint
//        cell.selectedBackgroundView = bgColorView
        
        
        return cell
    }

    
    func Gologin()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var rootController = UIViewController()
        rootController = storyboard.instantiateViewController(withIdentifier: "loginVC") as! loginVC
        
        sideMenuController?.embed(centerViewController: rootController)
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
        
        selectindex = indexPath.row
        
        let cell = tableView.cellForRow(at: indexPath) as! SideCellTableViewCell
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = themeColorFaint
        cell.selectedBackgroundView = bgColorView
        
        
        switch (menuArray.object(at: indexPath.row) as? String) {
        case  "Home":
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
           sideMenuController?.embed(centerViewController: rootController)
            
           case  "My Profile":
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     var rootController = UIViewController()
                      rootController = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                     sideMenuController?.embed(centerViewController: rootController)
      
        case "Terms & Conditions":
            
            UserDefaults.standard.set(true, forKey: "terms")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            
            sideMenuController?.embed(centerViewController: rootController)
            
            case "Earnings":
                          
                                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                 var rootController = UIViewController()
                                 rootController = storyboard.instantiateViewController(withIdentifier: "IncomeVC") as! IncomeVC
                                 
                                 sideMenuController?.embed(centerViewController: rootController)
            
            case "Instructions":
                       
                       UserDefaults.standard.set(false, forKey: "terms")

                       let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       var rootController = UIViewController()
                       rootController = storyboard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
                       
                       sideMenuController?.embed(centerViewController: rootController)
            
            
         case "My Id":
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "myIdVC") as! myIdVC
            
            sideMenuController?.embed(centerViewController: rootController)
            
            
        case "Order Accepted List":
                      
                      
                      let storyboard = UIStoryboard(name: "Main", bundle: nil)
                      var rootController = UIViewController()
                      rootController = storyboard.instantiateViewController(withIdentifier: "OrderAcceptedVC") as! OrderAcceptedVC
                      
                      UserDefaults.standard.set(accept_order_list, forKey: "methodName")
                      UserDefaults.standard.set("Accepted Orders", forKey: "headingName")

                     
                      sideMenuController?.embed(centerViewController: rootController)
                      
        case "Order Delivered List":
                                        
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        var rootController = UIViewController()
                                        rootController = storyboard.instantiateViewController(withIdentifier: "OrderAcceptedVC") as! OrderAcceptedVC
    
    UserDefaults.standard.set(delivered_list, forKey: "methodName")
                         UserDefaults.standard.set("Delivered Orders", forKey: "headingName")
    
    
                                        
                                        sideMenuController?.embed(centerViewController: rootController)
                      
                      
         case "Order Return List":
                                                   
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                   var rootController = UIViewController()
                                                   rootController = storyboard.instantiateViewController(withIdentifier: "OrderAcceptedVC") as! OrderAcceptedVC
               
               UserDefaults.standard.set(return_list, forKey: "methodName")
               UserDefaults.standard.set("Returned Orders", forKey: "headingName")
               
                 sideMenuController?.embed(centerViewController: rootController)
            
            case "Order InTransit List":
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 var rootController = UIViewController()
              rootController = storyboard.instantiateViewController(withIdentifier: "OrderAcceptedVC") as! OrderAcceptedVC
                                 
                      UserDefaults.standard.set(in_transit_list, forKey: "methodName")
                UserDefaults.standard.set("In Transit Orders", forKey: "headingName")
                                 
              sideMenuController?.embed(centerViewController: rootController)
      
        case "Logout":
            
            let alertController = UIAlertController(title: "LOGOUT ALERT", message: "Are you sure, you want to logout", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                UIAlertAction in
                    NSLog("OK Pressed")
                
                
              UserDefaults.standard.removeObject(forKey: "userIdLogin")
                
                    self.Gologin()
                
             
            }
            let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.destructive) {
                UIAlertAction in
                NSLog("Cancel Pressed")
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        

          
        default:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var rootController = UIViewController()
            rootController = storyboard.instantiateViewController(withIdentifier: "CustomSideMenuController")
            sideMenuController?.embed(centerViewController: rootController)

        }
    }
}

//
//  IncomeVC.swift
//  Med2u Driver
//
//  Created by USER on 17/07/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class IncomeVC: UIViewController {
    
    
    // MARK: - iBOutlets
    
    
    @IBOutlet weak var grad_view: UIView!
    @IBOutlet weak var withdraw_popup: UIView!
    
    
    
    @IBOutlet weak var lifetimeEarn_lbl: UILabel!
    @IBOutlet weak var currentEarn_lbl: UILabel!
    @IBOutlet weak var amount_tf: UITextField!
    
    
    @IBOutlet weak var withdraw: UIButton!
    // MARK: - iBActions
    
    
    @IBAction func withdraw_act(_ sender: Any) {
        
        self.withdraw_popup.isHidden = false
    }
    
    @IBAction func menu_act(_ sender: Any) {
        sideMenuController?.toggle()
    }
    
    @IBAction func ok_act(_ sender: Any) {
      WithdrawEarning()
    }
    
    @IBAction func cross_act(_ sender: Any) {
        
        self.withdraw_popup.isHidden = true

    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
                 if #available(iOS 13.0, *) {
                                let app = UIApplication.shared
                                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                                
                                let statusbarView = UIView()
                                statusbarView.backgroundColor = themeColorFaint
                                view.addSubview(statusbarView)
                              
                                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                                statusbarView.heightAnchor
                                    .constraint(equalToConstant: statusBarHeight).isActive = true
                                statusbarView.widthAnchor
                                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                                statusbarView.topAnchor
                                    .constraint(equalTo: view.topAnchor).isActive = true
                                statusbarView.centerXAnchor
                                    .constraint(equalTo: view.centerXAnchor).isActive = true
                              
                            } else {
                                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                                statusBar?.backgroundColor = themeColorFaint
                            }
             
        //For setting grdaient :-
             
                    self.grad_view.clipsToBounds = true
                    self.withdraw.clipsToBounds = true

        
                    let gradientLayer: CAGradientLayer = CAGradientLayer()
                    gradientLayer.frame = view.bounds
                    let topColor: CGColor = themeColorFaint.cgColor
                    let middleColor: CGColor = themeColorDark.cgColor
                  let bottomColor: CGColor = themeColorDark.cgColor
                    gradientLayer.colors = [topColor, middleColor, bottomColor]
                    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                    gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                          self.grad_view.applyGradient(colours: [themeColorFaint, themeColorDark])

              self.withdraw.layer.insertSublayer(gradientLayer, at: 0)
DriverDetails()
        
    }
    
    func  WithdrawEarning()
          {
               let type = NVActivityIndicatorType.ballClipRotateMultiple
                                      let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                      let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                             self.view.addSubview(activityIndicatorView)
                            self.view.isUserInteractionEnabled = false
               
              
                   activityIndicatorView.startAnimating()
                   var param = [String:Any]()
         
        param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
            param["amount"] =  self.amount_tf.text as! String
        

         WebService().postRequest(methodName: withdrawal_earn , parameter: param) { (response) in
                       
                       activityIndicatorView.stopAnimating()
                      self.view.isUserInteractionEnabled = true
                       
                       if let newResponse = response as? NSDictionary {
                           if newResponse.value(forKey: "status") as! Bool  == true {
                               
                  self.withdraw_popup.isHidden = true
                            self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)


               }
                      
                         
                           else {
                               self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                           }
                       }
                       else if let newMsg = response as? String{
                          self.view.showToast(toastMessage: newMsg, duration: 1)
                       }
                       else {
                          self.view.showToast(toastMessage: "No Data Found", duration: 1)

                       }
                   }
               
          }
    
    
     func  DriverDetails()
                        {
                             let type = NVActivityIndicatorType.ballClipRotateMultiple
                                                    let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                                    let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                                           self.view.addSubview(activityIndicatorView)
                                          self.view.isUserInteractionEnabled = false
                             
                            
                                 activityIndicatorView.startAnimating()
                                 var param = [String:Any]()
                     
                          param["user_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
                           
                           
                                 WebService().postRequest(methodName: get_detail, parameter: param) { (response) in
                                     
                                     activityIndicatorView.stopAnimating()
                                    self.view.isUserInteractionEnabled = true
                                     
                                     if let newResponse = response as? NSDictionary {
                                         if newResponse.value(forKey: "status") as! Bool  == true {
                                          
                                          let userData = newResponse.value(forKey: "data") as! NSDictionary
                                            
                                            let lifetm = userData.value(forKey: "wallet") as! String
                                            let earning = userData.value(forKey: "earning") as! String

                                      
                                            self.lifetimeEarn_lbl.text = "$ " + lifetm
                                            
                                          self.currentEarn_lbl.text = "$ " + earning
                                            
                                            
                                             }
                                      else {
                                          
                                  self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                                    }
                                         }
                                      
                                     else {
                                        self.view.showToast(toastMessage: "No Data Found", duration: 1)

                                     }
                                 }
                             
                        }
                 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

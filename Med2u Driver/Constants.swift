//
//  Constants.swift
//  Teammates.net
//
//  Created by CP-02 on 05/12/18.
//  Copyright © 2018 CP-02. All rights reserved.
//

import Foundation
import AVKit




let google_APIkey = "AIzaSyDpLGc9pTTxvx4qAraD9bPV7oDgfBIpnj8"

let googleLanguage = "en"

let img_Url = "https://med2u.com.au/"
let base_url = "https://med2u.com.au/api/"

let RegistrationService = "Driver/signup"
let LoginService = "Driver/login"
let logout = "Driver/logout"
let pharmacy_list = "pharmacy_list"
let update_profile = "Driver/update_profile"
let order_text = "order_text"
let order_scan = "order_scan"
let terms_driver = "Driver/terms_driver"
let instruction_driver = "Driver/instruction_driver"

let job_list = "Driver/job_list2"
let get_detail = "Driver/get_detail"

let accept_order = "Driver/accept_order"

let accept_order_list = "Driver/accept_order_list"
let in_transit_list = "Driver/in_transit_list"
let delivered_list = "Driver/delivered_list"
let return_list = "Driver/return_list"

let delivered_and_return = "Driver/delivered_and_return"
let forgot_password = "H1/forgot_password"
let withdrawal_earn = "Driver/withdrawal_earn"
let current_lat_long = "Driver/current_lat_long"


let themeColorFaint = hexStringToUIColor (hex:"8eb1cd")
let themeColorDark = hexStringToUIColor (hex:"2f5b7f")
let themeColorNavy = hexStringToUIColor (hex:"1B1F3D")


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

//
//  OrderAcceptedVC.swift
//  Med2u Driver
//
//  Created by CW-21 on 03/06/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OrderAcceptedVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
     
    
     @IBOutlet weak var heading_lbl: UILabel!
        
        
        @IBOutlet weak var table_view: UITableView!
        @IBOutlet weak var grad_view: UIView!

       
        //MARK : IBACTIONS:
      
      @IBAction func menu_act(_ sender: Any) {
          sideMenuController?.toggle()
      }
    
    
    var OrderData = NSArray()
    var method = String()
    var heading = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
                       let app = UIApplication.shared
                       let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                       
                       let statusbarView = UIView()
                       statusbarView.backgroundColor = themeColorFaint
                       view.addSubview(statusbarView)
                     
                       statusbarView.translatesAutoresizingMaskIntoConstraints = false
                       statusbarView.heightAnchor
                           .constraint(equalToConstant: statusBarHeight).isActive = true
                       statusbarView.widthAnchor
                           .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                       statusbarView.topAnchor
                           .constraint(equalTo: view.topAnchor).isActive = true
                       statusbarView.centerXAnchor
                           .constraint(equalTo: view.centerXAnchor).isActive = true
                     
                   } else {
                       let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                       statusBar?.backgroundColor = themeColorFaint
                   }
        
        
        
        
        
                            let gradientLayer: CAGradientLayer = CAGradientLayer()
                              gradientLayer.frame = view.bounds
                                let topColor: CGColor = themeColorFaint.cgColor
                                let middleColor: CGColor = themeColorDark.cgColor
                               let bottomColor: CGColor = themeColorDark.cgColor
                                gradientLayer.colors = [topColor, middleColor, bottomColor]
                                   gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                                  gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                 
                    self.grad_view.clipsToBounds = true
                               self.grad_view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        method = UserDefaults.standard.value(forKey: "methodName") as! String
         heading = UserDefaults.standard.value(forKey: "headingName") as! String
        
        
        heading_lbl.text = heading

            
       
    }
    override func viewWillAppear(_ animated: Bool) {
        OrderListing()
    }
     // MARK: - TABLE VIEW METHODS :-
//        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//            return 290
//
//        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return OrderData.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrederacceptedCell
            
    //        cell.img_vw.contentMode = .scaleToFill
    //        cell.img_vw.image = UIImage.init(named: "pharmacy")
    //        cell.img_vw.backgroundColor = .white
    let dict = OrderData[indexPath.row] as! NSDictionary

          cell.name_lbl.text = dict.value(forKey: "name") as! String
            cell.distance_lbl.textAlignment = .right
            let orderid = dict.value(forKey: "id") as! String
            let distance = dict.value(forKey: "distance") as! String
            
            cell.orderid_lbl.text = "Order Id:- " + orderid
            cell.address_lbl.text = dict.value(forKey: "address1") as! String
            cell.distance_lbl.text = distance + " Km"
            cell.number_lbl.text = dict.value(forKey: "mobile") as! String

        
            
           cell.selectionStyle = .none
            
            
            return cell
        
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
   let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
    
                 let dict = OrderData[indexPath.row] as! NSDictionary
    
                     vc.dict = dict
    
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    
    // MARK: - Webservice Calling

    func  OrderListing()
          {
               let type = NVActivityIndicatorType.ballClipRotateMultiple
                                      let frame = CGRect(x: self.view.frame.size.width/2-40, y: self.view.frame.size.height/2-40, width: 80, height: 80)
                                      let activityIndicatorView =   NVActivityIndicatorView(frame: frame, type: type, color: themeColorNavy, padding: 20)
                             self.view.addSubview(activityIndicatorView)
                            self.view.isUserInteractionEnabled = false
               
              
                   activityIndicatorView.startAnimating()
                   var param = [String:Any]()
       
            param["driver_id"] = UserDefaults.standard.value(forKey: "userIdLogin")
             
             WebService().postRequest(methodName: method, parameter: param) { (response) in
                    
                        activityIndicatorView.stopAnimating()
                
                
                      self.view.isUserInteractionEnabled = true
                       
                       if let newResponse = response as? NSDictionary {
                           if newResponse.value(forKey: "status") as! Bool  == true {
                            
                            let userData = newResponse.value(forKey: "data") as! NSArray
                            
                            
                              
                             self.OrderData = userData
                            print("self.OrderData",self.OrderData)

                             
                             self.table_view.delegate = self
                             
                             self.table_view.dataSource = self
                             
                             self.table_view.reloadData()
                            
                               }
                        else {
                            
                    self.view.showToast(toastMessage: newResponse.value(forKey: "message") as! String, duration: 1)

                                                      }
                           }
                        
                       else {
                          self.view.showToast(toastMessage: "No Data Found", duration: 1)

                       }
                   }
               
          }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

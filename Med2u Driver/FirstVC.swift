//
//  FirstVC.swift
//  MED 2U
//
//  Created by CW-21 on 04/04/20.
//  Copyright © 2020 CW-21. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class FirstVC: UIViewController {
    
    //MARK : IBOUTLETS :
    
    
    @IBOutlet weak var Signin_btn: UIButton!
    
     //MARK : IBACTIONS :
    
    @IBAction func signin_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    } 
    
    @IBAction func signup_act(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK : IBDECLATIONS :


    override func viewDidLoad() {
        super.viewDidLoad()
        
    if #available(iOS 13.0, *) {
                   let app = UIApplication.shared
                   let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                   
                   let statusbarView = UIView()
                   statusbarView.backgroundColor = themeColorFaint
                   view.addSubview(statusbarView)
                 
                   statusbarView.translatesAutoresizingMaskIntoConstraints = false
                   statusbarView.heightAnchor
                       .constraint(equalToConstant: statusBarHeight).isActive = true
                   statusbarView.widthAnchor
                       .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                   statusbarView.topAnchor
                       .constraint(equalTo: view.topAnchor).isActive = true
                   statusbarView.centerXAnchor
                       .constraint(equalTo: view.centerXAnchor).isActive = true
                 
               } else {
                   let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                   statusBar?.backgroundColor = themeColorFaint
               }

  // self.Signin_btn.applyGradient(colours: [themeColorFaint, themeColorDark])
        
      //For setting grdaient :-
                     
                            self.Signin_btn.clipsToBounds = true
                            let gradientLayer: CAGradientLayer = CAGradientLayer()
                            gradientLayer.frame = view.bounds
                            let topColor: CGColor = themeColorFaint.cgColor
                            let middleColor: CGColor = themeColorDark.cgColor
                          let bottomColor: CGColor = themeColorDark.cgColor
                            gradientLayer.colors = [topColor, middleColor, bottomColor]
                            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                            self.Signin_btn.layer.insertSublayer(gradientLayer, at: 0)
       
    }
    override func viewDidLayoutSubviews() {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = Signin_btn.bounds
//        gradientLayer.colors = [themeColorFaint, themeColorDark]
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//
//        // Render the gradient to UIImage
//        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
//        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        //  Set the UIImage as background property
//        self.Signin_btn.setBackgroundImage(image, for: .normal)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}
extension UIView {
    func insertHorizontalGradient(_ color1: UIColor, _ color2: UIColor) -> GradientView {
        let gradientView = GradientView(frame: bounds)
        gradientView.color1 = color1
        gradientView.color2 = color2
        gradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
       // self.insertSubview(gradientView, at 0)
        return gradientView
    }
}

//
//  CustomSideMenuController.swift
//  Hook
//
//  Created by CP-02 on 19/12/18.
//  Copyright © 2018 CP-02. All rights reserved.
//

import UIKit
import SideMenuController


@available(iOS 13.0, *)
class CustomSideMenuController: SideMenuController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
        performSegue(withIdentifier: "embedSideController", sender: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .horizontalPan
        SideMenuController.preferences.animating.transitionAnimator = FadeAnimator.self
        SideMenuController.preferences.interaction.swipingEnabled = false
        SideMenuController.preferences.drawing.sidePanelWidth = UIScreen.main.bounds.width*0.75//(appDelegate.window?.frame.size.width)!-50
        super.init(coder: aDecoder)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
